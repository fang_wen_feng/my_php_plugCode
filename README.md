# 说明
### 简要描述
><p>一些自己写的或二次封装的PHP类或函数（文件上传、图片处理、mysql数据库的连接、JWT、多级列表）。还有一些是网上收集的代码（二维码、验证码、邮件发送、phpQuery、PHPExcel、PHPPowerPoint、PhpWord）。</p>
### 自己觉得好用的代码推荐
>&emsp;&emsp;mysql数据库连接的文件: phpcode分支里面 phpCode/PDO_MYSQL.php、DB_MYSQL.php、DB_MYSQLI.php。<br>
>&emsp;&emsp;多级列表功能的： phpcode分支里面 phpCode/List_tree.php 。<br>
>&emsp;&emsp;JWT: Jwt.php 。<br>
>&emsp;&emsp;收集或自己封装的函数代码： phpcode分支里面的 phpCode/public_functions.php、configs_helper.php、sendMail.php <br>
>&emsp;&emsp;二维码、验证码、phpQuery、PHPExcel、PHPPowerPoint、PhpWord ： phpCode分支 phpCode/files/*.php
### 一些代码文件的功能描述
<p>
&emsp;&emsp;<b>phpcode分支里面的 phpCode/public_functions.php这个代码文件描述，功能有：</b><br>
&emsp;&emsp;数据类型的判断，随机字符串生成，curl方式的请求，递归查看目录列表，遍历删除目录和目录下所有文件，字符串截取，字符串长度获取，字符串分隔，数组的维度获取，数组排序（一般用于数据库中请求的二维数组排序），获取文件扩展名，判断是否SSL协议，数据过滤函数，分级列表处理功能的函数，分页，数字编码转换为字符，字符转换为数字编码，浏览器友好的变量输出，判断是否是移动端访问等。
</p>

