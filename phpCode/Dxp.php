<?php

/**
 * 简单的对DOM进行查询
 * Class Dxp
 */
class Dxp
{
    private $dom = '';
    private $dxp = '';
    public $allEle = array();

    public function __construct($html = '')
    {
        //$args = func_get_args();
        $this->dom = new DOMDocument();
        $this->dom->loadHTML($html);
        $this->dxp = new DOMXPath($this->dom);
        return self::init('', $this->dom);
        //return call_user_func_array(array($this, "init"), $args);
    }

    public function init($html = '', $dom = '')
    {
        $this->allEle = array();
        $this->dom = '';
        $this->dxp = '';
        if (self::is_string($html) || is_object($dom)) {
            if ($dom instanceof DOMDocument) {
                $this->dom = $dom;
            } else {
                $this->dom = new DOMDocument();
            }
            self::is_string($html) && $this->dom->loadHTML($html);
            $this->dxp = new DOMXPath($this->dom);
            $eleAll = $this->dom->getElementsByTagName("*");
            for ($i = 0; $i < $eleAll->length; $i++) {
                $val = $eleAll->item($i);
                $this->allEle[] = array(
                    "path" => $val->getNodePath(),
                    "tagName" => $val->tagName ? $val->tagName : $val->nodeName,
                    "attributes" => self::attributes($val),
                    "parentTagName" => $val->parentNode->tagName,
                    "textContent" => $val->textContent ? $val->textContent : $val->nodeValue,
                    "parentNode" => self::element($val->parentNode),
                    "childElement" => self::childElement($val),
                    //"self" => $val,
                );
            }
        }
        return $this;
    }

    /**
     * 对象类型的元素列表数据转为数组
     * @param object $list
     * @param array $del_kyes
     * @return array
     */
    public function obj_ele_list2array($list, $del_kyes = '')
    {
        if (is_object($list) && $list->length) {
            for ($i = 0; $i < $list->length; $i++) {
                $val = $list->item($i);
                if ($val->tagName) {
                    $ele = array(
                        "path" => $val->getNodePath(),
                        "tagName" => $val->tagName ? $val->tagName : $val->nodeName,
                        "attributes" => self::attributes($val),
                        "textContent" => $val->textContent ? $val->textContent : $val->nodeValue,
                        "parentNode" => self::element($val->parentNode),
                        "childElement" => self::childElement($val),
                        //"self" => $val,
                    );
                    self::del_array_keys($ele, $del_kyes);
                    $new_arr[] = $ele;
                }
            }
        }
        return $new_arr;
    }

    /**
     * 构建属性列表数据
     * @param object $ele
     * @return bool
     */
    public function attributes($ele)
    {
        if (is_object($ele) && $ele->attributes) {
            for ($i = 0; $i < $ele->attributes->length; $i++) {
                $val = $ele->attributes->item($i);
                $attr = $val->name ? $val->name : $val->nodeName;
                $arr[$attr] = array(
                    "name" => $attr,
                    "value" => $val->value ? $val->value : $val->nodeValue,
                    "textContent" => $val->textContent,
                );
            }
            return $arr;
        }
        return false;
    }

    /**
     * 获取元素里面的某个属性值
     * @param string|object|array $ele
     * @param string|callable $attr
     * @return bool
     */
    public function getAttrVal($ele = '', $attr = '')
    {
        //$ele = (new DOMDocument())->getElementsByTagName("*")->item(0);
        if (is_object($ele->attributes)) {
            for ($i = 0; $i < $ele->attributes->length; $i++) {
                $val = $ele->attributes->item($i);
                $name = $val->name ? $val->name : $val->nodeName;
                $value = $val->value ? $val->value : $val->nodeValue;
                if (is_callable($attr)) {
                    if (in_array($attr($val, $name, $value), array(false, 0, "", null), true)) {
                        return $value;
                    }
                } else {
                    if ($attr == $name) {
                        return $value;
                    }
                }
            }
        } elseif (is_array($ele['attributes']) && self::arrayLevel($ele['attributes']) >= 2) {
            if (is_callable($attr)) {
                foreach ($ele['attributes'] as $key => $val) {
                    if (in_array($attr($ele['attributes'], $key, $val), array(false, 0, "", null), true)) {
                        return $val['value'];
                    }
                }
            }
            return $ele['attributes'][$attr]['value'];
        }
        return false;
    }

    /**
     * 构建元素数据
     * @param $ele
     * @return array|bool
     */
    final public function element($ele)
    {
        if (is_object($ele) && $ele->tagName) {
            return array(
                "path" => $ele->getNodePath(),
                "tagName" => $ele->tagName,
                "attributes" => self::attributes($ele),
                "textContent" => $ele->textContent ?: $ele->nodeValue,
                //"parentNode" => self::element($ele->parentNode),
                //"self" => $ele,
            );
        }
        return false;
    }

    /**
     * 整理获取元素的子元素列表
     * @param $ele
     * @return array|bool
     */
    final public function childElement($ele)
    {
        if (is_object($ele) && $ele->childNodes) {
            for ($i = 0; $i < $ele->childNodes->length; $i++) {
                $val = $ele->childNodes->item($i);
                $temp = self::element($val);
                $temp && ($arr[] = $temp);
            }
            return $arr;
        }
        return false;
    }

    /**
     * 查询元素，建议css查询层级越少越好，目前只支持如下：
     * 子元素查询: ele > ele1 、 ele > .class1 、 ele > #id1 等
     * 后代选择：ele ele1 或 ele .class 等
     * 属性选择：ele[attr=val] 、ele[attr^=val] 、ele[attr$=val] 、ele[attr*=val] 、 [attr=val]、[attr^=val]等
     * 分组查询：ele,ele1,ele2 等，能不用分组就不用。
     * 联合查询(多类查询)：ele.class1.class2 、 ele.class1#id1 等。
     * @param string $xpath
     * @param bool $is_css_select
     * @param array $del_keys
     * @return array|bool
     */
    public function query($xpath = '', $is_css_select = false, $del_keys = array('parentNode', 'childElement'))
    {
        if (self::is_string($xpath)) {
            if ($is_css_select) {
                $select = self::handle_css_select_string($xpath);
                $lv2_arr = $this->allEle;
                $prev_ele_list = $temp = array();
                if (is_array($select['group'])) {
                    // xxx,xx1,xx2
                    foreach ($select['group'] as $index => $select1) {
                        is_array($s_ele_list = self::query($select1, $is_css_select, $del_keys)) && ($temp = array_merge($temp, $s_ele_list));
                    }
                    return $temp;
                } else {
                    if (is_array($select['split']) && is_array($select['mode'])) {
                        $n = -1;
                        foreach ($select['split'] as $index => $item) {
                            if ($select['mode'][$n] === " ") {
                                $temp = self::handle_css_select_ele($item, " ", $prev_ele_list, $lv2_arr, $del_keys);
                            } elseif ($select['mode'][$n] === ">") {
                                $temp = self::handle_css_select_ele($item, ">", $prev_ele_list, $lv2_arr, $del_keys);
                            } else {
                                $temp = self::handle_css_select_ele($item, " ", $prev_ele_list, $lv2_arr, $del_keys);
                            }
                            $n++;
                        }
                        return $temp;
                    }
                }
            } else {
                $id_class = "[A-Za-z_][A-Za-z_\-0-9]+";
                $xpath = preg_replace(
                    array("/\s{2,}/", "/(>|\s{0,2}>\s{0,2})/", "/\s{0,2}=\s{0,2}/", "/\[\s{0,2}/", "/\s{0,2}\]/", "/\s+/", "/(\.{$id_class})/i", "/(#{$id_class})/i"),
                    array(" ", "/", "=", "[@", "]", "//", "*[@class='\$1']", "*[@id='\$1']"),
                    $xpath);
                $xpath = str_replace(array("='.", "='#", "^=", "$=", "*="), array("='", "='", "=", "=", "="), $xpath);
                $xpath = "//$xpath";
                return self::obj_ele_list2array($this->dxp->query($xpath), $del_keys);
            }
        }
        return false;
    }

    /**
     * 处理部分css选择器字符串
     * @param string $xpath
     * @return array
     */
    public function handle_css_select_string($xpath = '')
    {
        $rep1 = array(' ', '>', '=', '^', '$', '*', ',');
        $rep2 = array('☖', '♻', '☗', '☘', '☢', '☯', '⚐');
        $xpath = self::str_replace_callback(
            array(
                '0' => "/(\[[\s\S]+?\])/i",
                '1' => "/(\s*>\s*)/",
            ),
            array(
                '0' => function ($matches) use ($xpath, $rep1, $rep2) {
                    $list = preg_split('/\s*=\s*/i', $matches[0], 2);
                    if (is_array($list) && count($list) > 1) {
                        $list[0] = str_replace(' ', '', $list[0]);
                        $list[1] = str_replace($rep1, $rep2, "='" . trim($list[1], "\"']") . "']");
                        return join('', $list);
                    }
                    return preg_replace('/\s+/', '', $matches[0]);
                },
                '1' => function ($matches) use ($xpath) {
                    return '>';
                },
            ), $xpath);
        $select = preg_replace(array("/(\s{2,})/", "/\s{0,2},\s{0,2}/"), array(" ", ","), $xpath);
        $preg = array('mode' => '/(\s|>)/i', 'group' => '/,/');
        preg_match_all($preg['mode'], $select, $str_list);
        $split = preg_split($preg['mode'], $select);
        foreach ($split as $index => $v) {
            $split[$index] = str_replace($rep2, $rep1, $v);
        }
        $group_split = false;
        if (strpos($select, ',') > 0) {
            $group_split = preg_split($preg['group'], $select);
            foreach ($group_split as $index => $v) {
                $group_split[$index] = str_replace($rep2, $rep1, $v);
            }
        }

        return array('select' => str_replace($rep2, $rep1, $select), 'split' => $split, "mode" => $str_list[0], "group" => $group_split);
    }

    /**
     * 元素筛选
     * @param string $tag
     * @param string $sTag
     * @param string|array $prev_ele_list
     * @param string|array $lv2_arr
     * @return array
     */
    private function handle_css_select_ele($tag = '', $sTag = '', &$prev_ele_list = '', $lv2_arr = '', $del_keys = array('parentNode', 'childElement'))
    {
        is_array($lv2_arr) || ($lv2_arr = $this->allEle);
        $tag_preg = array(
            "tagName" => "/^[A-Za-z_\-][A-Za-z0-9_\-]*$/i",
            "class" => "/^\.[A-Za-z0-9_\-]+$/i",
            "id" => "/^#[A-Za-z0-9_\-]+$/i",
            "attr" => "/^[.#]{0,1}[A-Za-z0-9_\-\*]*\[\S+\]$/i",
            "joint" => "/^[.#]{0,1}[A-Za-z0-9_\-]+([.#][A-Za-z0-9_\-]+)+$/i", //div.class 或 .class.class1
        );
        $temp = array();
        $path_list = array();
        $fn1 = function ($tagName1, $attrs1, $v1, &$temp) use ($tag_preg, $tag, $sTag) {
            if (preg_match($tag_preg['tagName'], $tag)) {
                ($tagName1 == $tag) && ($temp[] = $v1);
            } elseif (preg_match($tag_preg['class'], $tag)) {
                $class_list = preg_split('/\s+/', $attrs1['class']['value']);
                in_array(ltrim($tag, '. '), $class_list) && ($temp[] = $v1);
            } elseif (preg_match($tag_preg['id'], $tag)) {
                $id_list = preg_split('/\s+/', $attrs1['id']['value']);
                in_array(ltrim($tag, '# '), $id_list) && ($temp[] = $v1);
            } elseif (preg_match($tag_preg['joint'], $tag)) {
                $arr = self::decide_css_str_id_class_tag($tag);
                foreach ($arr as $val) {
                    if ($val['type'] === 'tagName') {
                        $res[] = $tagName1 === $val['val'];
                    } elseif ($val['type'] === 'id') {
                        $id_list = preg_split('/\s+/', $attrs1['id']['value']);
                        $res[] = in_array($val['val'], $id_list);
                    } elseif ($val['type'] === 'class') {
                        $class_list = preg_split('/\s+/', $attrs1['class']['value']);
                        $res[] = in_array($val['val'], $class_list);
                    }
                }
                in_array(false, $res) || ($temp[] = $v1);
            } elseif (preg_match($tag_preg['attr'], $tag)) {
                $list = preg_split('/(\^=|\$=|\*=|=)/', $tag, 2);
                $_attr = preg_split('/\[/', $list[0], 2); // 标签,属性名
                $_attr['ok'] = false;
                (strpos($_attr[0], '.') === 0) && ($_attr['ok'] = ($v1['attributes']['class']['value'] === ltrim($_attr[0], '.')));
                (strpos($_attr[0], '#') === 0) && ($_attr['ok'] = ($v1['attributes']['id']['value'] === ltrim($_attr[0], '#')));
                $_attr['ok'] = ($_attr['ok'] || $v1['tagName'] == $_attr[0] || $_attr[0] === '' || $_attr[0] === '*');
                $attrName = trim($_attr[1], '[');
                $attrVal_temp = $attrVal = trim($list[1], '\']');
                $attrVal = self::str_escape($attrVal);
                if (preg_match('/\^=/', $tag)) {
                    preg_match("/^$attrVal/", $attrs1[$attrName]['value']) && $_attr['ok'] && ($temp[] = $v1);
                } elseif (preg_match('/\$=/', $tag)) {
                    preg_match("/$attrVal\$/", $attrs1[$attrName]['value']) && $_attr['ok'] && ($temp[] = $v1);
                } elseif (preg_match('/\*=/', $tag)) {
                    preg_match("/$attrVal/", $attrs1[$attrName]['value']) && $_attr['ok'] && ($temp[] = $v1);
                } elseif (preg_match('/\=/', $tag)) {
                    ($attrVal_temp == $attrs1[$attrName]['value'] && $_attr['ok']) && ($temp[] = $v1);
                }
            } elseif ($tag == "*") {
                $temp[] = $v1;
            }
        };
        (!is_array($prev_ele_list) || (is_array($prev_ele_list) && count($prev_ele_list) < 1)) && ($prev_ele_list = array($this->allEle[0]));
        if (is_array($prev_ele_list)) {
            foreach ($prev_ele_list as $k => $v) {
                $path = self::str_escape($v['path']);
                $tagName = $v['tagName'];
                $attrs = $v['attributes'];
                foreach ($lv2_arr as $k1 => $v1) {
                    $tagName1 = $v1['tagName'];
                    $attrs1 = $v1['attributes'];
                    self::del_array_keys($v1, $del_keys);
                    if (preg_match("/^$path\//", $v1['path'])) {
                        if ($sTag == " ") {
                            $fn1($tagName1, $attrs1, $v1, $temp);
                        } elseif ($sTag == ">" && preg_match("/^{$path}\/[A-Za-z0-9_\-]+(\[\d+\])*$/i", $v1['path'])) {
                            $fn1($tagName1, $attrs1, $v1, $temp);
                        }
                    }
                }
            }
            //去重
            foreach ($temp as $k => $v) {
                if (in_array($v['path'], $path_list)) {
                    unset($temp[$k]);
                }
                $path_list[] = $v['path'];
            }
        }
        $prev_ele_list = $temp;
        return $temp;
    }

    /**
     * 元素列表查询，返回二维数组
     * @param string|callable $name
     * @param string $opt
     * @param string $value
     * @param bool $is_attributes
     * @param string|array $lv2_arr
     * @return array
     */
    public function ele_list_query($name = '', $opt = '', $value = '', $is_attributes = false, $lv2_arr = '')
    {
        $ele_list = (is_array($lv2_arr) && self::arrayLevel($lv2_arr) >= 2) ? $lv2_arr : $this->allEle;
        if (is_callable($name)) {
            foreach ($ele_list as $index => $item) {
                if (in_array($name($ele_list, $index, $item), array(true, 1, '1'), true)) {
                    $new_ele_list[] = $item;
                }
            }
        } else {
            foreach ($ele_list as $index => $item) {
                if (self::is_string($name) && self::is_string($opt) && self::is_string($value)) {
                    if ($opt === "=") {
                        if ($is_attributes) {
                            ($item['attributes'][$name]['value'] === $value) && ($new_ele_list[] = $item);
                        } else {
                            ($item[$name] === $value) && ($new_ele_list[] = $item);
                        }
                    } elseif ($opt === "^=") {
                        if ($is_attributes) {
                            preg_match("/^$value/i", $item['attributes'][$name]['value']) && ($new_ele_list[] = $item);
                        } else {
                            preg_match("/^$value/i", $item[$name]) && ($new_ele_list[] = $item);
                        }
                    } elseif ($opt === "$=") {
                        if ($is_attributes) {
                            preg_match("/$value$/i", $item['attributes'][$name]['value']) && ($new_ele_list[] = $item);
                        } else {
                            preg_match("/$value$/i", $item[$name]) && ($new_ele_list[] = $item);
                        }
                    } elseif ($opt === "*=") {
                        if ($is_attributes) {
                            preg_match("/$value/i", $item['attributes'][$name]['value']) && ($new_ele_list[] = $item);
                        } else {
                            preg_match("/$value/i", $item[$name]) && ($new_ele_list[] = $item);
                        }
                    }
                } elseif (self::is_string($name) && self::is_string($item) && !$value) {
                    if ($is_attributes) {
                        ($item['attributes'][$name]['value'] === $opt) && ($new_ele_list[] = $item);
                    } else {
                        ($item[$name] === $opt) && ($new_ele_list[] = $item);
                    }
                } elseif (self::is_string($name) && !$opt && !$value) {
                    if ($is_attributes) {
                        ($item['attributes'][$name]['value']) && ($new_ele_list[] = $item);
                    } else {
                        ($item[$name]) && ($new_ele_list[] = $item);
                    }
                }
            }
        }
        return $new_ele_list;
    }

    /**
     * 是否是字符串
     * @param string|int|double $str
     * @return bool
     */
    final public function is_string($str = '')
    {
        return !in_array($str, array('', null, false), true) && (is_string($str) || is_numeric($str));
    }

    /**
     * 判断是否存在
     * @param $var
     * @param array $opt 这里的值代表了不存在的意思
     * @return bool
     */
    static final public function exist($var, $opt = array("", null, false))
    {
        isset($var) || ($var = false);
        return !in_array($var, $opt, true);
    }

    /**
     * 判断是否是关联数组
     * @param $arr
     * @return bool
     */
    static final public function is_assoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * 浏览器友好的变量输出
     * @param mixed $var 变量
     * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
     * @param string $label 标签 默认为空
     * @param boolean $strict 是否严谨 默认为true
     * @return void|string
     */
    public function dump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            } else {
                $output = $label . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else
            return $output;
    }

    /**
     * 返回数组的维度
     * @param $arr
     * @return mixed
     */
    final public function arrayLevel($arr)
    {
        $al = array(0);
        self::aL($arr, $al);
        return max($al);
    }

    static final private function aL($arr, &$al, $level = 0)
    {
        if (is_array($arr)) {
            $level++;
            $al[] = $level;
            foreach ($arr as $k => $v) {
                self::aL($v, $al, $level);
            }
        }
    }

    /**
     * curl get 请求
     * @param string $url
     * @param array $options 关联数组(一维)，如：[CURLOPT_URL => 'http://www.example.com/',CURLOPT_POST => 1]
     * @param int $CURLOPT_TIMEOUT 设置cURL允许执行的最长秒数
     * @param callable $success_fn
     * @param callable $fail_fn
     * @return mixed
     */
    final public function curlGet($url = '', $options = array(), $CURLOPT_TIMEOUT = 30, $success_fn = '', $fail_fn = '')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $CURLOPT_TIMEOUT);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            if (is_callable($fail_fn)) {
                $fail_fn($ch);
            } else {
                //打印错误信息
                var_dump(curl_error($ch));
            }
        } else {
            is_callable($success_fn) && $success_fn($ch);
        }
        curl_close($ch);
        return $data;
    }

    /**
     * @param string $url
     * @param string|array $postData post请求传输的数据
     * @param array $options 关联数组(一维)，如：[CURLOPT_URL => 'http://www.example.com/',CURLOPT_POST => 1]
     * @param int $CURLOPT_TIMEOUT 设置cURL允许执行的最长秒数
     * @param callable $success_fn
     * @param callable $fail_fn
     * @return mixed
     */
    final public function curlPost($url = '', $postData = '', $options = array(), $CURLOPT_TIMEOUT = 30, $success_fn = '', $fail_fn = '')
    {
        if (is_array($postData)) {
            $postData = http_build_query($postData);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, $CURLOPT_TIMEOUT); //设置cURL允许执行的最长秒数
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            is_callable($fail_fn) && $fail_fn($ch);
        } else {
            is_callable($success_fn) && $success_fn($ch);
        }
        curl_close($ch);
        return $data;
    }

    /**
     * @param string|string[] $pattern
     * @param string|callable|callable[] $callback
     * @param string|string[] $subject
     * @param int $limit
     * @return mixed|null|string|string[]
     */
    public function str_replace_callback($pattern = '', $callback = '', $subject = '', $limit = -1)
    {
        if (self::is_string($pattern) && is_callable($callback) && self::is_string($subject)) {
            return preg_replace_callback($pattern, $callback, $subject, $limit);
        } elseif (is_array($pattern) && is_array($callback) && self::is_string($subject)) {
            foreach ($pattern as $key => $val) {
                is_callable($callback[$key]) && ($subject = preg_replace_callback($val, $callback[$key], $subject, $limit));
            }
            return $subject;
        } else {
            $args = func_get_args();
            return call_user_func_array('preg_replace_callback', $args);
        }
    }

    /**
     * 数字编码转换为字符
     * @param $code
     * @return string
     */
    final public function chr_utf8($code)
    {
        if ($code < 128) {
            $utf = chr($code);
        } else if ($code < 2048) {
            $utf = chr(192 + (($code - ($code % 64)) / 64));
            $utf .= chr(128 + ($code % 64));
        } else {
            $utf = chr(224 + (($code - ($code % 4096)) / 4096));
            $utf .= chr(128 + ((($code % 4096) - ($code % 64)) / 64));
            $utf .= chr(128 + ($code % 64));
        }
        return $utf;
    }

    /**
     * 字符转换为数字编码
     * @param $string
     * @param $offset
     * @return float|int
     */
    final public function ord_utf8($string, &$offset)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) {
            if ($code < 224) {
                //otherwise 0xxxxxxx
                $bytesnumber = 2;
            } else if ($code < 240) {
                //110xxxxx
                $bytesnumber = 3;
            } else if ($code < 248) {
                //1110xxxx
                $bytesnumber = 4;
            }
            //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                //10xxxxxx
                $code2 = ord(substr($string, $offset, 1)) - 128;
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) {
            $offset = -1;
        }
        return $code;
    }

    /**
     *把字符串分割为数组(一维数组)
     *@$str string  分割的字符串
     *@$charset string 字符串编码
     */
    function str_cut($str, $charset = 'utf-8')
    {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        return $match[0];
    }

    /**
     * 字符串转义
     * @param string $string
     * @param array|string $str_list
     * @param int $n
     * @return bool|mixed
     */
    public function str_escape($string = '', $str_list = '', $n = 1)
    {
        $str_list || ($str_list = array('/', '[', ']', '{', '}', '(', ')', '+', '*', '.', '^', '$', '|'));
        if (self::is_string($str_list)) {
            $str_list = self::str_cut($str_list);
        }
        if (is_array($str_list)) {
            $rep_str = array();
            $escape = '\\';
            ($n == 2) && ($escape .= $escape);
            foreach ($str_list as $index => $val) {
                $rep_str[] = $escape . $val;
            }
            return str_replace($str_list, $rep_str, $string);
        }
        return false;
    }

    public function del_array_keys(&$arr, $keys)
    {
        if (is_array($keys) && count($keys) > 0) {
            foreach ($keys as $key) {
                unset($arr[$key]);
            }
        }
        return $arr;
    }

    /**
     * 判断css选择器片段区分id，class，tagName
     * @param string $str
     * @return array
     */
    public function decide_css_str_id_class_tag($str = '')
    {
        $type = array('', 'tagName');
        $zd = array('@.' => array('.', 'class'), '@#' => array('#', 'id'), '@' => array('', 'tagName'));
        strpos($str, '.') === 0 && ($type = array('.', 'class'));
        strpos($str, '#') === 0 && ($type = array('#', 'id'));
        preg_match_all("/([\.#])/i", ltrim($str, '.#'), $list);
        (is_array($list[0]) && count($list[0]) > 0) && ($split = preg_split('/[\.#]+/', ltrim($str, '.#')));
        $re[] = array('type' => $type[1], 'prefix' => $type[0], 'slice' => $type[0] . $split[0], 'val' => $split[0]);
        for ($i = 1, $j = 0, $len = count($split); $i < $len; $i++, $j++) {
            $prefix = $list[0][$j];
            $re[] = array('type' => $zd["@$prefix"][1], 'prefix' => $prefix, 'slice' => $prefix . $split[$i], 'val' => $split[$i]);
        }
        return $re;
    }

    /**
     * 获取HTML代码
     * @param string|array $ele_list
     * @return array|string
     */
    public function getHTML($ele_list = '')
    {
        if (is_array($ele_list) && count($ele_list) > 0) {
            $html = array();
            foreach ($ele_list as $index => $item) {
                $val = $this->dxp->query($item['path'])->item(0);
                $html[] = self::createElementCode($val);
            }
            return $html;
        }
        return '';
    }

    /**
     * 还原为HTML代码
     * @param DOMElement|DOMNodeList $ele
     * @return string
     */
    public function createElementCode($ele){
        if ($ele instanceof DOMElement) {
            $attr_code = "";
            if($ele->attributes){
                for ($i = 0, $len = $ele->attributes->length; $i < $len; $i++) {
                    $val = $ele->attributes->item($i);
                    $attr = $val->name ? $val->name : $val->nodeName;
                    $attrVal = $val->nodeValue ?: $val->textContent;
                    $attr_code .= " {$attr}=\"{$attrVal}\"";
                }
                $attr_code = self::is_string($attr_code) ? " $attr_code" : $attr_code;
            }
            $body = self::createElementCode($ele->childNodes) ?: $ele->textContent;
            $html = "<{$ele->tagName}{$attr_code}>{$body}</{$ele->tagName}>";
            return $html;
        } elseif ($ele instanceof DOMNodeList) {
            //childNodes
            $html = "";
            for ($i = 0, $len = $ele->length; $i < $len; $i++) {
                $val = $ele->item($i);
                if($val instanceof DOMElement){
                    $html .= self::createElementCode($val);
                } elseif ($val instanceof DOMText){
                    $html .= $val->nodeValue;
                }
            }
            return $html;
        }
        return "";
    }
}