<?php

/**
 * 使用openssl加密解密
 * openssl_encrypt(
 * string $data,  待加密的明文信息数据
 * string $cipher_algo,  加密算法
 * string $passphrase,  密钥
 * int $options = 0,  [0:自动对明文进行 padding, 返回的数据经过 base64 编码|1:OPENSSL_RAW_DATA, 自动对明文进行 padding, 但返回的结果未经过 base64 编码|2:OPENSSL_ZERO_PADDING, 自动对明文进行 0 填充, 返回的结果经过 base64 编码]
 * string $iv = "",  非 NULL 的初始化向量
 * string &$tag = null,
 * string $aad = "",
 * int $tag_length = 16
 * ): string|false
 *
 * Class Aes
 */
class Aes
{
    static private $config = array(
        //签名算法
        'sign_algorithm' => 'sha256',
    );

    /**
     * 设置数组, 如: set_array($arr,'lv1.lv2',$value); set_array($arr,array('lv1','lv2'),$value);
     * @param $arr
     * @param string|array $name
     * @param string $value
     * @param int $index
     * @return mixed
     */
    static public function set_array(&$arr, $name = '', $value = '', $index = 0)
    {
        if ($name && is_array($name)) {
            $len = count($name);
            if ($len > 0 && $index < $len) {
                if (!isset($arr[$name[$index]]) || !is_array($arr[$name[$index]])) {
                    $arr[$name[$index]] = array();
                    if ($index == $len - 1) {
                        $arr[$name[$index]] = $value;
                        return $arr;
                    }
                }
                self::set_array($arr[$name[$index]], $name, $value, $index + 1);
            }
        } elseif ($name && is_string($name) && strpos($name, '.') > 0) {
            $name = explode('.', $name);
            self::set_array($arr, $name, $value, $index);
        } else {
            $arr[$name] = $value;
        }
    }

    /**
     * 获取数组 如： get_array($arr,'lv1.lv2'); get_array($arr,array('lv1','lv2'));
     * @param $arr
     * @param string|array $name
     * @param int $index
     * @return mixed
     */
    static public function get_array(&$arr, $name, $index = 0)
    {
        if ($name && is_array($name)) {
            $len = count($name);
            if ($len > 0 && $index < $len) {
                if ($index == $len - 1) {
                    return $arr[$name[$index]];
                }
                return self::get_array($arr[$name[$index]], $name, (int)$index + 1);
            }
        } elseif ($name && is_string($name) && strpos($name, '.') > 0) {
            $name = explode('.', $name);
            return self::get_array($arr, $name, $index);
        } else {
            return $name ? $arr[$name] : $arr;
        }
    }

    static public function set_config($key, $value = null)
    {
        if (is_array($key) && empty($value)) {
            foreach ($key as $k => $v) {
                self::$config[$k] = $v;
            }
        } else {
            self::set_array(self::$config, $key, $value);
        }
    }

    static public function get_config($name = '')
    {
        return self::get_array(self::$config, $name);
    }

    /**
     * 判断是否存在
     * @param $var
     * @param array $opt 这里的值代表了不存在的意思
     * @return bool
     */
    static final public function is_exist($var, $opt = array("", null, false))
    {
        isset($var) || ($var = false);
        if (in_array($var, $opt, true)) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否是字符串
     * @param $var
     * @return bool
     */
    static public function is_string($var)
    {
        return self::is_exist($var) && (is_string($var) || is_numeric($var) || is_double($var));
    }

    /**
     * 获取变量值
     * @return bool|*
     */
    static final public function get_vars()
    {
        $args = func_get_args();
        $var = false;
        foreach ($args as $arg) {
            if (is_exist($arg)) {
                $var = $arg;
                break;
            }
        }
        return $var;
    }

    /**
     * 浏览器友好的变量输出
     * @param mixed $var 变量
     * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
     * @param string $label 标签 默认为空
     * @param boolean $strict 是否严谨 默认为true
     * @return void|string
     */
    static public function dump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            } else {
                $output = $label . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else
            return $output;
    }

    static public function encode64($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    static public function decode64($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     *把字符串分割为数组(一维数组)
     *@$str string  分割的字符串
     *@$charset string 字符串编码
     */
    static final public function str_cut($str, $charset = 'utf-8')
    {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        return $match[0];
    }

    /**
     * 返回随机字符串
     * @param int $l
     * @param string $mode
     * @param array $config
     * @param string $charset
     * @return string
     */
    static public function randomString($l = 5, $mode = "n", $config = array(), $charset = "utf-8")
    {
        $C = array(
            "n" => "0123456789",
            "s" => "abcdefghijklmnopqrstuvwxyz",
            "S" => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "ns" => "0123456789abcdefghijklmnopqrstuvwxyz",
            "nS" => "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "sS" => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "nsS" => "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "hex" => "0123456789abcdef",
            "all" => "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_^+*=|,.~!@#",
        );
        if (is_array($config) && count($config) > 0) {
            foreach ($config as $key => $value) {
                $C[$key] = self::str_cut($value, $charset);
            }
        }
        $mode = empty($C[$mode]) ? "n" : $mode;
        $str = "";
        if (is_array($C[$mode]) && count($C[$mode]) > 0) {
            for ($i = 0, $len = count($C[$mode]); $i < $l; $i++) {
                $str .= $C[$mode][mt_rand(0, $len - 1)];
            }
        } else {
            for ($i = 0, $len = strlen($C[$mode]); $i < $l; $i++) {
                $str .= $C[$mode][mt_rand(0, $len - 1)];
            }
        }
        return $str;
    }

    /**
     * 设置签名算法
     * @param string $algo
     */
    static public function set_sign_algo($algo = 'sha256')
    {
        self::is_string($algo) && (self::$config['sign_algorithm'] = $algo);
    }

    /**
     * aes-xxx-cbc 加密
     * @param string $data 数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return string
     */
    static public function aes_num_cbc_encrypt($data = '', $key = '', $hex_iv = '', $cipher_algo = 'aes-256-cbc')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cbc';
        $sign_algo = self::$config['sign_algorithm'];
        $iv_length = openssl_cipher_iv_length($cipher_algo);
        $hex_iv_is_hex = preg_match('/^[0-9a-f]+$/', $hex_iv);
        if ($hex_iv_is_hex) {
            $hex_iv_length = $iv_length * 2;
            (strlen($hex_iv) < $hex_iv_length) && ($hex_iv = str_pad($hex_iv, $hex_iv_length, '0'));
            (strlen($hex_iv) > $hex_iv_length) && ($hex_iv = substr($hex_iv, 0, $hex_iv_length));
            $iv = hex2bin($hex_iv);
        } else {
            $iv = openssl_random_pseudo_bytes($iv_length);
        }
        $encrypt_str = openssl_encrypt($data, $cipher_algo, $key, 0, $iv);
        $sign = hash_hmac($sign_algo, $encrypt_str, $key, true);
        $list = $hex_iv_is_hex ? array($encrypt_str, self::encode64($sign)) : array(self::encode64($iv), $encrypt_str, self::encode64($sign));
        return self::encode64(join('.', $list));
    }

    /**
     * aes-xxx-cbc 解密
     * @param string $en_data 加密的数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return bool|string
     */
    static public function aes_num_cbc_decrypt($en_data = '', $key = '', $hex_iv = '', $cipher_algo = 'aes-256-cbc')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cbc';
        $sign_algo = self::$config['sign_algorithm'];
        $iv_length = openssl_cipher_iv_length($cipher_algo);
        $hex_iv_is_hex = preg_match('/^[0-9a-f]+$/', $hex_iv);
        $en_data = self::decode64($en_data);
        $list = explode('.', $en_data);
        if ($hex_iv_is_hex) {
            $hex_iv_length = $iv_length * 2;
            (strlen($hex_iv) < $hex_iv_length) && ($hex_iv = str_pad($hex_iv, $hex_iv_length, '0'));
            (strlen($hex_iv) > $hex_iv_length) && ($hex_iv = substr($hex_iv, 0, $hex_iv_length));
            $iv = hex2bin($hex_iv);
            $encrypt_str = $list[0];
            $sign = self::decode64($list[1]);
        } else {
            $iv = self::decode64($list[0]);
            $encrypt_str = $list[1];
            $sign = self::decode64($list[2]);
        }
        $decrypt_str = openssl_decrypt($encrypt_str, $cipher_algo, $key, 0, $iv);
        if (hash_equals($sign, hash_hmac($sign_algo, $encrypt_str, $key, true))) {
            return $decrypt_str;
        } else {
            return false;
        }
    }

    static public function aes_256_cbc_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-256-cbc');
    }

    static public function aes_256_cbc_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-256-cbc');
    }

    static public function aes_192_cbc_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-192-cbc');
    }

    static public function aes_192_cbc_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-192-cbc');
    }

    static public function aes_128_cbc_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-128-cbc');
    }

    static public function aes_128_cbc_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-128-cbc');
    }

    static public function aes_256_cbc_hmac_sha256_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-256-cbc-hmac-sha256');
    }

    static public function aes_256_cbc_hmac_sha256_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-256-cbc-hmac-sha256');
    }

    static public function aes_128_cbc_hmac_sha256_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-128-cbc-hmac-sha256');
    }

    static public function aes_128_cbc_hmac_sha256_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-128-cbc-hmac-sha256');
    }

    static public function aes_256_cbc_hmac_sha1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-256-cbc-hmac-sha1');
    }

    static public function aes_256_cbc_hmac_sha1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-256-cbc-hmac-sha1');
    }

    static public function aes_128_cbc_hmac_sha1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-128-cbc-hmac-sha1');
    }

    static public function aes_128_cbc_hmac_sha1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-128-cbc-hmac-sha1');
    }

    /**
     * aes-xxx-ecb 加密
     * @param string $data 数据
     * @param string $key 密钥
     * @param string $cipher_algo 密码算法
     * @return string
     */
    static public function aes_num_ecb_encrypt($data = '', $key = '', $cipher_algo = 'aes-256-ecb')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-ecb';
        $sign_algo = self::$config['sign_algorithm'];
        $encrypt_str = openssl_encrypt($data, $cipher_algo, $key);
        $sign = hash_hmac($sign_algo, $encrypt_str, $key, true);
        $list = array($encrypt_str, self::encode64($sign));
        return self::encode64(join('.', $list));
    }

    /**
     * aes-xxx-ecb 解密
     * @param string $en_data 加密的数据
     * @param string $key 密钥
     * @param string $cipher_algo 密码算法
     * @return bool|string
     */
    static public function aes_num_ecb_decrypt($en_data = '', $key = '', $cipher_algo = 'aes-256-ecb')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-ecb';
        $sign_algo = self::$config['sign_algorithm'];
        $en_data = self::decode64($en_data);
        $list = explode('.', $en_data);
        $encrypt_str = $list[0];
        $sign = self::decode64($list[1]);
        $decrypt_str = openssl_decrypt($encrypt_str, $cipher_algo, $key);
        if (hash_equals($sign, hash_hmac($sign_algo, $encrypt_str, $key, true))) {
            return $decrypt_str;
        } else {
            return false;
        }
    }

    static public function aes_256_ecb_encrypt($data = '', $key = '')
    {
        return self::aes_num_ecb_encrypt($data, $key, 'aes-256-ecb');
    }

    static public function aes_256_ecb_decrypt($en_data = '', $key = '')
    {
        return self::aes_num_ecb_decrypt($en_data, $key, 'aes-256-ecb');
    }

    static public function aes_192_ecb_encrypt($data = '', $key = '')
    {
        return self::aes_num_ecb_encrypt($data, $key, 'aes-192-ecb');
    }

    static public function aes_192_ecb_decrypt($en_data = '', $key = '')
    {
        return self::aes_num_ecb_decrypt($en_data, $key, 'aes-192-ecb');
    }

    static public function aes_128_ecb_encrypt($data = '', $key = '')
    {
        return self::aes_num_ecb_encrypt($data, $key, 'aes-128-ecb');
    }

    static public function aes_128_ecb_decrypt($en_data = '', $key = '')
    {
        return self::aes_num_ecb_decrypt($en_data, $key, 'aes-128-ecb');
    }

    /**
     * aes-xxx-cfb 加密
     * @param string $data 数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return string
     */
    static public function aes_num_cfb_encrypt($data = '', $key = '', $hex_iv = '', $cipher_algo = '')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cfb';
        $sign_algo = self::$config['sign_algorithm'];
        $iv_length = openssl_cipher_iv_length($cipher_algo);
        $hex_iv_is_hex = preg_match('/^[0-9a-f]+$/', $hex_iv);
        if ($hex_iv_is_hex) {
            $hex_iv_length = $iv_length * 2;
            (strlen($hex_iv) < $hex_iv_length) && ($hex_iv = str_pad($hex_iv, $hex_iv_length, '0'));
            (strlen($hex_iv) > $hex_iv_length) && ($hex_iv = substr($hex_iv, 0, $hex_iv_length));
            $iv = hex2bin($hex_iv);
        } else {
            $iv = openssl_random_pseudo_bytes($iv_length);
        }
        $encrypt_str = openssl_encrypt($data, $cipher_algo, $key, 0, $iv);
        $sign = hash_hmac($sign_algo, $encrypt_str, $key, true);
        $list = $hex_iv_is_hex ? array($encrypt_str, self::encode64($sign)) : array(self::encode64($iv), $encrypt_str, self::encode64($sign));
        return self::encode64(join('.', $list));
    }

    /**
     * aes-xxx-cfb 解密
     * @param string $en_data 加密的数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return bool|string
     */
    static public function aes_num_cfb_decrypt($en_data = '', $key = '', $hex_iv = '', $cipher_algo = '')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cfb';
        $sign_algo = self::$config['sign_algorithm'];
        $iv_length = openssl_cipher_iv_length($cipher_algo);
        $hex_iv_is_hex = preg_match('/^[0-9a-f]+$/', $hex_iv);
        $en_data = self::decode64($en_data);
        $list = explode('.', $en_data);
        if ($hex_iv_is_hex) {
            $hex_iv_length = $iv_length * 2;
            (strlen($hex_iv) < $hex_iv_length) && ($hex_iv = str_pad($hex_iv, $hex_iv_length, '0'));
            (strlen($hex_iv) > $hex_iv_length) && ($hex_iv = substr($hex_iv, 0, $hex_iv_length));
            $iv = hex2bin($hex_iv);
            $encrypt_str = $list[0];
            $sign = self::decode64($list[1]);
        } else {
            $iv = self::decode64($list[0]);
            $encrypt_str = $list[1];
            $sign = self::decode64($list[2]);
        }
        $decrypt_str = openssl_decrypt($encrypt_str, $cipher_algo, $key, 0, $iv);
        if (hash_equals($sign, hash_hmac($sign_algo, $encrypt_str, $key, true))) {
            return $decrypt_str;
        } else {
            return false;
        }
    }

    static public function aes_256_cfb_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-256-cfb');
    }

    static public function aes_256_cfb_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-256-cfb');
    }

    static public function aes_192_cfb_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-192-cfb');
    }

    static public function aes_192_cfb_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-192-cfb');
    }

    static public function aes_128_cfb_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-128-cfb');
    }

    static public function aes_128_cfb_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-128-cfb');
    }

    static public function aes_256_cfb1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-256-cfb1');
    }

    static public function aes_256_cfb1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-256-cfb1');
    }

    static public function aes_192_cfb1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-192-cfb1');
    }

    static public function aes_192_cfb1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-192-cfb1');
    }

    static public function aes_128_cfb1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-128-cfb1');
    }

    static public function aes_128_cfb1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-128-cfb1');
    }

    static public function aes_256_cfb8_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-256-cfb8');
    }

    static public function aes_256_cfb8_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-256-cfb8');
    }

    static public function aes_192_cfb8_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-192-cfb8');
    }

    static public function aes_192_cfb8_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-192-cfb8');
    }

    static public function aes_128_cfb8_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-128-cfb8');
    }

    static public function aes_128_cfb8_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-128-cfb8');
    }


    /**
     * 数据填充
     * @param string $data
     * @param int $group_length 分组长度
     * @return string
     */
    static public function padding($data = '', $group_length = 16)
    {
        //需要补充的长度
        $padding = $group_length - (strlen($data) % $group_length);
        //填充字符，并且该字符记录了填充的长度
        $pad_str = chr($padding);
        return $data . str_repeat($pad_str, $padding);
    }

    /**
     * 清除末尾填充的字符
     * @param string $pad_data
     * @return bool|string
     */
    static public function un_padding($pad_data = '')
    {
        //获取填充的字符
        $pad_str = substr($pad_data, -1);
        //获取填充的长度
        $padding = ord($pad_str);
        if ($padding > strlen($pad_data)) {
            return false;
        }
        if (strspn($pad_data, $pad_str, -($padding), $padding) !== $padding) {
            return false;
        }
        return substr($pad_data, 0, -($padding));
    }

}