class Aes01
{
    static public function encode64($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    static public function decode64($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * 是否是字符串
     * @param string|int|double $str
     * @return bool
     */
    static public function is_string($str = '')
    {
        return !in_array($str, array('', null, false), true) && (is_string($str) || is_numeric($str));
    }

    /**
     * 是否是十六进制字符串
     * @param string $str
     * @return bool
     */
    static public function is_hex($str = '')
    {
        return !!preg_match('/^[0-9a-f]+$/', $str);
    }

    /**
     * 把字符串分割为数组(一维数组)
     * @param string $str
     * @param string $charset
     * @return mixed
     */
    static public function str_cut($str, $charset = 'utf-8')
    {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        return $match[0];
    }

    /**
     * 返回随机字符串
     * @param int $l
     * @param string $mode
     * @param array $config
     * @param string $charset
     * @return string
     */
    static public function randomString($l = 5, $mode = "n", $config = array(), $charset = "utf-8")
    {
        $C = array(
            "n" => "0123456789",
            "s" => "abcdefghijklmnopqrstuvwxyz",
            "S" => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "ns" => "0123456789abcdefghijklmnopqrstuvwxyz",
            "nS" => "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "sS" => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "nsS" => "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "hex" => "0123456789abcdef",
            "all" => "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_^+*=|,.~!@#",
        );
        if (is_array($config) && count($config) > 0) {
            foreach ($config as $key => $value) {
                $C[$key] = self::str_cut($value, $charset);
            }
        }
        $mode = empty($C[$mode]) ? "n" : $mode;
        $str = "";
        if (is_array($C[$mode]) && count($C[$mode]) > 0) {
            for ($i = 0, $len = count($C[$mode]); $i < $l; $i++) {
                $str .= $C[$mode][mt_rand(0, $len - 1)];
            }
        } else {
            for ($i = 0, $len = strlen($C[$mode]); $i < $l; $i++) {
                $str .= $C[$mode][mt_rand(0, $len - 1)];
            }
        }
        return $str;
    }

    /**
     * 处理并返回密钥
     * @param string $key 密钥
     * @param string $cipher_algo 密码算法
     * @param int $bit 位数bit
     * @return bool|string
     */
    static public function str_key($key = '', $cipher_algo = '', $bit = 128)
    {
        if(self::is_string($cipher_algo)){
            $list = explode('-',$cipher_algo);
            $bit = intval($list[1]);
        }
        $len = ~~($bit / 8);
        $k_len = strlen($key);
        ($k_len < $len) && ($key = str_pad($key, $len, '0'));
        ($k_len > $len) && ($key = substr($key, 0, $len));
        return $key;
    }

    /**
     * 处理并返回十六进制字符串向量
     * @param string $hex_iv
     * @param string $cipher_algo
     * @return bool|string
     */
    static public function str_hex_iv($hex_iv = '', $cipher_algo = 'aes-256-cbc')
    {
        $iv_length = openssl_cipher_iv_length($cipher_algo);
        $hex_iv_length = $iv_length * 2;
        if (self::is_hex($hex_iv)) {
            (strlen($hex_iv) < $hex_iv_length) && ($hex_iv = str_pad($hex_iv, $hex_iv_length, '0'));
            (strlen($hex_iv) > $hex_iv_length) && ($hex_iv = substr($hex_iv, 0, $hex_iv_length));
        } else {
            $iv = openssl_random_pseudo_bytes($iv_length);
            $hex_iv = bin2hex($iv);
        }
        return $hex_iv;
    }

    /**
     * @param string|array $en_data
     * @param string $hex_iv
     * @param string $cipher_algo
     * @return array|bool
     */
    static public function get_en_data($en_data = '', $hex_iv = '', $cipher_algo = 'aes-256-cbc')
    {
        if (is_array($en_data)) {
            $encrypt_str = self::decode64($en_data[0]);
            $hex_iv = $en_data[1];
        } elseif (self::is_string($en_data)) {
            $encrypt_str = self::decode64($en_data);
            $hex_iv = self::str_hex_iv($hex_iv, $cipher_algo);
        } else {
            return false;
        }
        return array('hex_iv' => $hex_iv, 'encrypt_str' => $encrypt_str);
    }

    /**
     * aes-xxx-cbc 加密
     * @param string $data 数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return array
     */
    static public function aes_num_cbc_encrypt($data = '', $key = '', $hex_iv = '', $cipher_algo = 'aes-256-cbc')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cbc';
        $hex_iv = self::str_hex_iv($hex_iv, $cipher_algo);
        $iv = hex2bin($hex_iv);
        $key = self::str_key($key, $cipher_algo);
        $encrypt_str = openssl_encrypt($data, $cipher_algo, $key, 0, $iv);
        return array(self::encode64($encrypt_str), $hex_iv);
    }

    /**
     * aes-xxx-cbc 解密
     * @param string|array $en_data 加密的数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return bool|string
     */
    static public function aes_num_cbc_decrypt($en_data = '', $key = '', $hex_iv = '', $cipher_algo = 'aes-256-cbc')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cbc';
        $rv = self::get_en_data($en_data, $hex_iv, $cipher_algo);
        if ($rv) {
            $encrypt_str = $rv['encrypt_str'];
            $iv = hex2bin($rv['hex_iv']);
        } else {
            return false;
        }
        $key = self::str_key($key, $cipher_algo);
        $decrypt_str = openssl_decrypt($encrypt_str, $cipher_algo, $key, 0, $iv);
        return $decrypt_str;
    }

    static public function aes_256_cbc_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-256-cbc');
    }

    static public function aes_256_cbc_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-256-cbc');
    }

    static public function aes_192_cbc_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-192-cbc');
    }

    static public function aes_192_cbc_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-192-cbc');
    }

    static public function aes_128_cbc_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-128-cbc');
    }

    static public function aes_128_cbc_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-128-cbc');
    }

    static public function aes_256_cbc_hmac_sha256_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-256-cbc-hmac-sha256');
    }

    static public function aes_256_cbc_hmac_sha256_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-256-cbc-hmac-sha256');
    }

    static public function aes_128_cbc_hmac_sha256_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-128-cbc-hmac-sha256');
    }

    static public function aes_128_cbc_hmac_sha256_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-128-cbc-hmac-sha256');
    }

    static public function aes_256_cbc_hmac_sha1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-256-cbc-hmac-sha1');
    }

    static public function aes_256_cbc_hmac_sha1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-256-cbc-hmac-sha1');
    }

    static public function aes_128_cbc_hmac_sha1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_encrypt($data, $key, $hex_iv, 'aes-128-cbc-hmac-sha1');
    }

    static public function aes_128_cbc_hmac_sha1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cbc_decrypt($en_data, $key, $hex_iv, 'aes-128-cbc-hmac-sha1');
    }

    /**
     * aes-xxx-ecb 加密
     * @param string $data 数据
     * @param string $key 密钥
     * @param string $cipher_algo 密码算法
     * @return string
     */
    static public function aes_num_ecb_encrypt($data = '', $key = '', $cipher_algo = 'aes-256-ecb')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-ecb';
        $key = self::str_key($key, $cipher_algo);
        $encrypt_str = openssl_encrypt($data, $cipher_algo, $key);
        return self::encode64($encrypt_str);
    }

    /**
     * aes-xxx-ecb 解密
     * @param string $en_data 加密的数据
     * @param string $key 密钥
     * @param string $cipher_algo 密码算法
     * @return bool|string
     */
    static public function aes_num_ecb_decrypt($en_data = '', $key = '', $cipher_algo = 'aes-256-ecb')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-ecb';
        $encrypt_str = self::decode64($en_data);
        $key = self::str_key($key, $cipher_algo);
        $decrypt_str = openssl_decrypt($encrypt_str, $cipher_algo, $key);
        return $decrypt_str;
    }

    static public function aes_256_ecb_encrypt($data = '', $key = '')
    {
        return self::aes_num_ecb_encrypt($data, $key, 'aes-256-ecb');
    }

    static public function aes_256_ecb_decrypt($en_data = '', $key = '')
    {
        return self::aes_num_ecb_decrypt($en_data, $key, 'aes-256-ecb');
    }

    static public function aes_192_ecb_encrypt($data = '', $key = '')
    {
        return self::aes_num_ecb_encrypt($data, $key, 'aes-192-ecb');
    }

    static public function aes_192_ecb_decrypt($en_data = '', $key = '')
    {
        return self::aes_num_ecb_decrypt($en_data, $key, 'aes-192-ecb');
    }

    static public function aes_128_ecb_encrypt($data = '', $key = '')
    {
        return self::aes_num_ecb_encrypt($data, $key, 'aes-128-ecb');
    }

    static public function aes_128_ecb_decrypt($en_data = '', $key = '')
    {
        return self::aes_num_ecb_decrypt($en_data, $key, 'aes-128-ecb');
    }

    /**
     * aes-xxx-cfb 加密
     * @param string $data 数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return array
     */
    static public function aes_num_cfb_encrypt($data = '', $key = '', $hex_iv = '', $cipher_algo = '')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cfb';
        $hex_iv = self::str_hex_iv($hex_iv, $cipher_algo);
        $iv = hex2bin($hex_iv);
        $key = self::str_key($key, $cipher_algo);
        $encrypt_str = openssl_encrypt($data, $cipher_algo, $key, 0, $iv);
        return array(self::encode64($encrypt_str), $hex_iv);
    }

    /**
     * aes-xxx-cfb 解密
     * @param string|array $en_data 加密的数据
     * @param string $key 密钥
     * @param string $hex_iv iv初始化向量，十六进制字符串
     * @param string $cipher_algo 密码算法
     * @return bool|string
     */
    static public function aes_num_cfb_decrypt($en_data = '', $key = '', $hex_iv = '', $cipher_algo = '')
    {
        $cipher_algo = $cipher_algo ? $cipher_algo : 'aes-256-cfb';
        $rv = self::get_en_data($en_data, $hex_iv, $cipher_algo);
        if ($rv) {
            $encrypt_str = $rv['encrypt_str'];
            $iv = hex2bin($rv['hex_iv']);
        } else {
            return false;
        }
        $key = self::str_key($key, $cipher_algo);
        $decrypt_str = openssl_decrypt($encrypt_str, $cipher_algo, $key, 0, $iv);
        return $decrypt_str;
    }

    static public function aes_256_cfb_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-256-cfb');
    }

    static public function aes_256_cfb_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-256-cfb');
    }

    static public function aes_192_cfb_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-192-cfb');
    }

    static public function aes_192_cfb_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-192-cfb');
    }

    static public function aes_128_cfb_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-128-cfb');
    }

    static public function aes_128_cfb_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-128-cfb');
    }

    static public function aes_256_cfb1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-256-cfb1');
    }

    static public function aes_256_cfb1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-256-cfb1');
    }

    static public function aes_192_cfb1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-192-cfb1');
    }

    static public function aes_192_cfb1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-192-cfb1');
    }

    static public function aes_128_cfb1_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-128-cfb1');
    }

    static public function aes_128_cfb1_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-128-cfb1');
    }

    static public function aes_256_cfb8_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-256-cfb8');
    }

    static public function aes_256_cfb8_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-256-cfb8');
    }

    static public function aes_192_cfb8_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-192-cfb8');
    }

    static public function aes_192_cfb8_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-192-cfb8');
    }

    static public function aes_128_cfb8_encrypt($data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_encrypt($data, $key, $hex_iv, 'aes-128-cfb8');
    }

    static public function aes_128_cfb8_decrypt($en_data = '', $key = '', $hex_iv = '')
    {
        return self::aes_num_cfb_decrypt($en_data, $key, $hex_iv, 'aes-128-cfb8');
    }
}