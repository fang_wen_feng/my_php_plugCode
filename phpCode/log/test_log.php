<?php
include_once dirname(__FILE__)."/Log.php";
$l = new Log();
/*$lv1_str = $l->handle_data_string(['abc','bbb','ccc'],['abc'=>'abc1','ccc'=>'ccc1','bbb'=>'bbb1']);
$lv2_str = $l->handle_data_string(['x1'=>'abc','x2'=>'ddd','x0'=>'ccc'],[
    ['abc'=>'abc1','ddd'=>'ddd1','ccc'=>'ccc1'],
    ['abc'=>'abc2','ddd'=>'ddd2','ccc'=>'ccc2'],
]);
my_dump($l->handle_string_data(['abc','bbb','ccc'],$lv1_str));
my_dump($l->handle_string_data(['abc','ddd','ccc'],$lv2_str));*/
$log_file = date("Ymd");
//$l->init()->set_dir_path('/log-01/','G:\web\log2233')->data()->add($log_file);
//$l->init()->set('exp', 10)->expire(20)->set_dir_path('/log111')->data()->add($log_file);
$l->init()->set('exp', 20)->expire(20)->set_dir_path('/log111')->data()->add($log_file);
$log_file_path = rtrim($l->get('dir_path'),'/')."/$log_file.log";
$read = $l->read($log_file);
//$l->dump($l->read_dir_list('/','G:\virtual_machine'));exit();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="Keywords" content="关键词1,关键词2"><!--关键词-->
	<meta name="Description" content="描述"><!--描述-->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximun-scale=1.0"><!--宽度为设备宽度,初始缩放为1.0倍,最小缩放为1.0倍,最大缩放为1.0倍-->
	<!-- 设置浏览器不缓存 begin -->
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Cache-control" content="no-cache">
	<meta http-equiv="Cache" content="no-cache">
	<!-- 设置浏览器不缓存 end -->
	<title></title>
    <style>
        /*表格样式部分 start */
        .table_box{margin:10px 0 0 0;width:100%;border-collapse:collapse;}
        .table_box > thead > tr{height:40px;border:0.5px solid #d5d5d5;box-sizing:border-box;}
        .table_box > thead > tr > th{text-align:center;border:0.5px solid #e2e2e2;box-sizing:border-box;}
        .table_box > tbody > tr{height:35px;}
        .table_box > tbody > tr:nth-of-type(odd){background:#fafafa;}
        .table_box > tbody > tr:hover{background:#f1f1f1;}
        .table_box > tbody > tr > td{padding:0 0 0 5px;border:0.5px solid #e2e2e2;box-sizing:border-box;}
        /*表格样式部分 end */
    </style>
</head>
<body>
    <table class="table_box">
        <thead>
        <tr>
            <th>ip</th>
            <th>添加时间</th>
            <th>地址链接</th>
            <th>请求方式</th>
            <th>http版本</th>
            <th>用户信息(user_gent)</th>
            <th>accept</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ( $read['data'] as $k => $v): ?>
        <tr>
            <td><?php echos($v['ip']); ?></td>
            <td><?php echos($v['add_time']/*$v[0]*/); ?></td>
            <td><?php echos($v['url']); ?></td>
            <td><?php echos($v['method']); ?></td>
            <td><?php echos($v['protocol']); ?></td>
            <td><?php echos($v['user_agent']); ?></td>
            <td><?php echos($v['accept']); ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php
//$l->dump($l->get_body_data_assoc($log_file,0,3));
$l->dump($l->history_link($log_file));
?>
</body>
</html>