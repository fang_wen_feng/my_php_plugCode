<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2021/8/5
 * Time: 15:39
 */

class MySqlStringParser
{
    private $tableName = "";
    private $fields = "*";
    private $joins = "";
    private $wheres = "";
    private $groupBys = "";
    private $orders = "";
    private $limits = "";
    private $type = array(
        "insert" => "",
        "update" => "",
    );
    private $isGetSql = false;
    private $payload = array(
        "insert" => null,
        "update" => null,
    );

    /**
     * 获取数据类型
     * @param $var
     * @return string
     */
    public function get_type($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_callable($var)) return "function reference";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }

    /**
     * 返回数组的维度
     * @param $arr
     * @return mixed
     */
    public function arrayLevel($arr)
    {
        $al = array(0);
        self::aL($arr, $al);
        return max($al);
    }

    /**
     * 配合方法 self::arrayLevel
     * @param $arr
     * @param $al
     * @param int $level
     */
    private function aL($arr, &$al, $level = 0)
    {
        if (is_array($arr)) {
            $level++;
            $al[] = $level;
            foreach ($arr as $k => $v) {
                self::aL($v, $al, $level);
            }
        }
    }

    /**
     * 判断是否是关联数组
     * @param $arr
     * @return bool
     */
    public function is_assoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * 数据过滤函数
     * @param string|array $data 待过滤的字符串或字符串数组
     * @param bool $force 为true时忽略get_magic_quotes_gpc
     * @param bool $is_htmlspecialchars 为true时，防止被挂马，跨站攻击
     * @param bool $regexp 正则匹配转义字符
     * @return array|null|string|string[]
     */
    public function input($data, $force = false, $is_htmlspecialchars = false, $regexp = false)
    {
        if (is_string($data)) {
            $data = trim($is_htmlspecialchars ? htmlspecialchars($data) : $data);
            if (($force == true) || (!get_magic_quotes_gpc())) {
                $data = addslashes($data); // 防止sql注入
            }
            if ($regexp) {
                if (is_array($regexp)) {
                    $regexp = join('|', $regexp);
                }
                $data = preg_replace('/(' . $regexp . ')/', '\\\\$1', $data);
            }
            return $data;
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = self::input($value, $force, $is_htmlspecialchars, $regexp);
            }
            return $data;
        } else {
            return $data;
        }
    }

    /**
     * 浏览器友好的变量输出
     * @param mixed $var 变量
     * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
     * @param string $label 标签 默认为空
     * @param boolean $strict 是否严谨 默认为true
     * @return void|string
     */
    public function dump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            } else {
                $output = $label . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else {
            return $output;
        }
    }

    /**
     * 数组排序 使用例如： self::sortArrByManyField($lv2_array,'id',SORT_DESC,'field2',SORT_ASC)
     * @return mixed|null
     * @throws Exception
     */
    public function sortArrByManyField()
    {
        $args = func_get_args();
        if (empty($args)) {
            return null;
        }
        $arr = array_shift($args);
        if (!is_array($arr)) {
            throw new Exception("第一个参数不为数组");
        }
        foreach ($args as $key => $field) {
            if (is_string($field)) {
                $temp = array();
                foreach ($arr as $index => $val) {
                    $temp[$index] = $val[$field];
                }
                $args[$key] = $temp;
            }
        }
        $args[] = &$arr;//引用值
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }

    /**
     * 自定义判断某个变量值是否存在
     * @param $vars
     * @param array $exclude_value 排除项，值为这些的时候也当作存在
     * @return bool
     */
    public function exist($vars, $exclude_value = array(0, null, '0'))
    {
        if (is_numeric($vars) || is_int($vars) || is_float($vars)) {
            return true;
        }
        if (is_array($exclude_value) && count($exclude_value) > 0) {
            foreach ($exclude_value as $index => $item) {
                if ($vars === $item) {
                    return true;
                    break;
                }
            }
        }
        return $vars ? true : false;
    }

    /**
     * 获取客户端IP地址
     * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @return mixed
     */
    public function get_client_ip($type = 0)
    {
        $type = $type ? 1 : 0;
        static $ip = NULL;
        if ($ip !== NULL) return $ip[$type];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) unset($arr[$pos]);
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }

    /**
     * 获取二维数组里的某个字段的列值（一维数组）
     * @param array $array_level2 二维数组及多维数组
     * @param string $field
     * @param $newarray
     */
    public function field_array($array_level2, $field = '', &$newarray)
    {
        if (is_array($array_level2) && arrayLevel($array_level2) > 1) {
            foreach ($array_level2 as $key => $value) {
                if (isset($value[$field]) && $value[$field] !== '') {
                    $newarray[] = $value[$field];
                }
            }
        }
    }

    /**
     * @param array $array_level2 二维数组及多维数组
     * @param string $index_field 设置某个字段的值为 数组下标
     * @return array
     */
    public function reconfig_array($array_level2 = '', $index_field = 'id')
    {
        $arr = array();
        if (is_array($array_level2) && arrayLevel($array_level2) > 1) {
            foreach ($array_level2 as $k => $v) {
                $arr[$v[$index_field]] = $v;
            }
        }
        return $arr;
    }

    /**
     * 检测某个值是否存在 某个数组中，严格匹配
     * @param string|array|object|boolean|null $search_value
     * @param array $arr
     * @return bool
     */
    public function in_array($search_value = '', $arr = array())
    {
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                if ($search_value === $v) {
                    unset($arr, $k, $v, $search_value);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 返回毫秒
     * @return float
     */
    public function m_second()
    {
        $t = strval(microtime());
        $l = explode(' ', $t);
        $s = $l[1];
        $ms = ~~(floatval($l[0]) * 1000);
        $v = strval($s) . strval($ms);
        return (float)$v;
    }

    /**
     * 返回微秒
     * @return string
     */
    public function u_second()
    {
        $t = strval(microtime());
        $l = explode(' ', $t);
        $s = $l[1];
        $ms = ~~(floatval($l[0]) * 1000000);
        $v = strval($s) . strval($ms);
        return $v;
    }

    public function encode64($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function decode64($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * 把一维数组转换为特定字符串内容（用于sql语句）
     * @param string $lv1_array
     * @param string $name
     * @return string
     */
    public function array2string($lv1_array = '', $name = 'v')
    {
        $k = '';
        $v = '';
        $fields = '';
        if (is_array($lv1_array) && count($lv1_array) > 0) {
            foreach ($lv1_array as $key => $value) {
                $k .= "$key,";
                $v .= is_int($value) || is_float($value) ? "$value," : "'$value',";
                $fields .= is_int($value) || is_float($value) ? "$key=$value," : "$key='$value',";
            }
            $data['k'] = $k = trim($k, ',');
            $data['v'] = $v = trim($v, ',');
            $data['fields'] = $fields = trim($fields, ',');
            $data['insert'] = " ( $k )values( $v ) ";
            $data['update'] = " $fields ";
            $name = strtolower($name);
            return in_array($name, array('k', 'v', 'fields', 'insert', 'update')) ? $data[$name] : $data;
        } else {
            return '';
        }
    }

    /**
     * sql语句参数绑定（主要在 where 部分）
     * @param string $strings
     * @param array $bind
     * @return mixed|string
     */
    public function where_param_bind($strings = '', $bind = array())
    {
        if ($strings && is_string($strings)) {
            if (is_array($bind) && count($bind) > 0) {
                foreach ($bind as $key => $value) {
                    if (is_int($value) || is_float($value)) {
                        $strings = str_replace(":$key", "$value", $strings);
                    } else {
                        $strings = str_replace(":$key", "'$value'", $strings);
                    }
                }
            }
        }
        return $strings;
    }

    /**
     * 初始化变量（sql语句拼接）
     * @return $this
     */
    public function init()
    {
        $this->fields = "*";
        $this->joins = "";
        $this->wheres = "";
        $this->groupBys = "";
        $this->orders = "";
        $this->limits = "";
        $this->type = array(
            "insert" => "",
            "update" => "",
        );
        $this->isGetSql = false;
        $this->payload = array(
            "insert" => null,
            "update" => null,
        );
        return $this;
    }

    /**
     * 构建 where 条件语句
     * @param string|array $field
     * @param string|array $options
     * @param string|array $condition
     * @return $this
     */
    public function where($field = '', $options = '', $condition = '')
    {
        $args = func_get_args();
        $args_len = count($args);
        $where_str = '';
        if (is_array($field) && in_array(strtolower($options), array('or', 'and')) && !self::exist($condition)) {
            //$field 为数组的时候（1-3维数组）
            $arr_lv = self::arrayLevel($field);
            if ($arr_lv > 0 && $arr_lv < 4) {
                foreach ($field as $k => $v) {
                    if (is_array($v) && count($v) > 0) {
                        //['字段名', '选项表达式(=|<|>|<>|like|not like|regexp|not regexp|....)', '值', 'and|or']
                        $f = $v[0];
                        $opt = $v[1];
                        $val = $v[2];
                        $or_and = $v[3] ? strtoupper($v[3]) : 'AND';
                        if (in_array(strtolower(trim($opt)), array('in', 'not in'))) {
                            if (is_array($val)) {
                                $val = self::array2string($val, 'v');
                                $where_str .= "$f $opt ($val) $or_and ";
                            } else {
                                continue;
                            }
                        } else {
                            $where_str .= is_int($val) || is_float($val) ? "$f $opt $val $or_and " : "$f $opt '$val' $or_and ";
                        }
                    } else {
                        $or_and = $options ? strtoupper($options) : 'AND';
                        $where_str .= is_int($v) || is_float($v) ? "$k = $v $or_and " : "$k = '$v' $or_and ";
                    }
                }
                $where_str = rtrim(rtrim($where_str), $or_and);
                $this->wheres .= "($where_str)";
            }
        } elseif ($field && is_string($field) && in_array(strtolower($args[$args_len - 1]), array('or', 'and'))) {
            if ($args_len >= 3) {
                //同一字段多个查询条件,如： $this->where('name', ['like','abc%'], ['like','%haha%'], 'or')
                $or_and = strtoupper($args[$args_len - 1]);
                for ($i = 1, $len = $args_len - 1; $i < $len; $i++) {
                    if (is_array($args[$i]) && count($args[$i]) > 1) {
                        $opt = $args[$i][0];
                        $val = $args[$i][1];
                        if (is_array($val) && in_array(strtolower($opt), array('in', 'not in'))) {
                            $val = self::array2string($val, 'v');
                            $where_str .= "$field $opt ($val) $or_and ";
                        } elseif (is_int($val) || is_float($val)) {
                            $where_str .= "$field $opt $val $or_and ";
                        } else {
                            $where_str .= "$field $opt '$val' $or_and ";
                        }
                    }
                }
                $where_str = rtrim(rtrim($where_str), $or_and);
                $this->wheres .= "($where_str)";
            }
        } elseif (preg_match('/(\&|\|)/i', $field) && self::exist($options) && is_string($options) && self::exist($condition) && (is_array($condition) || is_string($condition))) {
            //多字段相同查询条件,如：$this->where('name|title', 'like', 'abc%');  $this->where('name&title', 'like', '%haha%');
            $preg = '/([a-zA-z0-9_]+\||[a-zA-z0-9_]+\&|[a-zA-z0-9_]+)/i';
            preg_match_all($preg, $field, $lv2_array);
            foreach ($lv2_array[0] as $k => $v) {
                if (strpos($v, '|') > 0) {
                    $new_array[] = array('field' => trim(trim($v), '|'), 'or_and' => 'OR', 'pre' => '|');
                } elseif (strpos($v, '&') > 0) {
                    $new_array[] = array('field' => trim(trim($v), '&'), 'or_and' => 'AND', 'pre' => '&');
                } else {
                    $new_array[] = array('field' => trim($v), 'or_and' => 'OR', 'pre' => '');
                }
            }
            foreach ($new_array as $k => $v) {
                $f = $v['field'];
                $or_and = $v['or_and'];
                if (is_array($condition) && in_array(strtolower($options), array('in', 'not in'))) {
                    $_condition = self::array2string($condition, 'v');
                    $where_str .= "$f $options ($_condition) $or_and ";
                } elseif (is_int($condition) || is_float($condition)) {
                    $where_str .= "$f $options $condition $or_and ";
                } else {
                    $where_str .= "$f $options '$condition' $or_and ";
                }
            }
            $where_str = rtrim(rtrim($where_str), $or_and);
            $this->wheres .= "($where_str)";

        } elseif ($field && is_string($field) && !is_object($options) && !self::exist($condition)) {
            if (is_array($options)) {
                //where 的字符串条件查询，也可以进行参数绑定(如： $this->where("field = :name", array("name"=>"value") ))
                $where_str = self::where_param_bind($field, $options);
            } elseif (self::exist($options) && !is_array($options)) {
                $where_str = is_int($options) || is_float($options) ? "$field = $options" : "$field = '$options'";
            } else {
                $where_str = $field;
            }
            $where_str = rtrim($where_str);
            $this->wheres .= "($where_str)";
        } else {
            if (is_array($condition) && in_array(strtolower($options), array('in', 'not in'))) {
                $condition = self::array2string($condition, 'v');
                $options = strtoupper($options);
                $this->wheres .= "$field $options ($condition) ";
            } elseif (is_int($condition) || is_float($condition)) {
                $this->wheres .= "$field $options $condition ";
            } else {
                $this->wheres .= "$field $options '$condition' ";
            }
        }
        $this->wheres = $this->wheres . " AND ";
        return $this;
    }

    public function whereOr()
    {
        $args = func_get_args();
        call_user_func_array(array($this, 'where'), $args);
        $this->wheres = rtrim(rtrim($this->wheres), 'AND') . 'OR ';
        return $this;
    }

    /**
     * 左连接
     * @param string $join 如：tableName t
     * @param string $condition 如：t.id = a.id
     * @return $this
     */
    public function leftJoin($join = '', $condition = '')
    {
        $this->joins .= "LEFT JOIN $join ON ($condition) ";
        return $this;
    }

    /**
     * 右连接
     * @param string $join 如：tableName t
     * @param string $condition 如：t.id = a.id
     * @return $this
     */
    public function rightJoin($join = '', $condition = '')
    {
        $this->joins .= "RIGHT JOIN $join ON ($condition) ";
        return $this;
    }

    /**
     * 连接(内连接)
     * @param string $join
     * @param string $condition
     * @return $this
     */
    public function join($join = '', $condition = '')
    {
        $this->joins .= "JOIN $join ON ($condition) ";
        return $this;
    }

    /**
     * 截取数据范围
     * @param string|int $offset 开始位置（从0开始）
     * @param string|int $length 截取的长度
     * @return $this
     */
    public function limit($offset = '', $length = '')
    {
        if ($offset !== '' || intval($offset) >= 0) {
            if (!empty($length)) {
                $this->limits = "LIMIT $offset ,$length";
            } else {
                $this->limits = "LIMIT $offset";
            }
        }
        return $this;
    }

    /**
     * 排序
     * @param string|array $field 字段名，数组时必须是一维关联数组
     * @param string $order 排序类型(desc|ASC)
     * @return $this
     */
    public function order($field, $order)
    {
        if (is_array($field)) {
            foreach ($field as $key => $value) {
                $this->orders .= "$key $value ,";
            }
        } else {
            $this->orders .= "$field $order ,";
        }
        return $this;
    }

    /**
     * 分组
     * @param string $field
     * @param bool $with_rollup 可以实现在分组统计数据基础上再进行相同的统计（SUM,AVG,COUNT…）
     * @return $this
     */
    public function groupBy($field = '', $with_rollup = false)
    {
        $this->groupBys = rtrim(" $field");
        ($with_rollup) and ($this->groupBys = " $field WITH ROLLUP");
        return $this;
    }

    /**
     * 表名
     * @param string $tableName 表名（tableName、db.tableName）
     * @return $this
     */
    public function table($tableName = '')
    {
        self::init();
        $this->tableName = " $tableName ";
        return $this;
    }

    /**
     * 查询的字段
     * @param string $field 字段（field1,field2,t.*,c.f1,c.f2..）
     * @return $this
     */
    public function field($field = '')
    {
        $this->fields = !empty($field) ? $field : "*";
        return $this;
    }

    /**
     * 更新语句(更新的数据需要使用SQL函数)
     * @param string|array $field
     * @param string $value
     * @return $this
     */
    public function exp($field = '', $value = '')
    {
        if (is_array($field)) {
            foreach ($field as $key => $v) {
                $this->exp($key, $v);
            }
        } elseif ($field !== '' && $value !== '') {
            $this->type['update'] = ltrim(trim($this->type['update']), ',');
            $this->type['update'] .= ",$field=$value";
        }
        return $this;
    }

    /**
     * 插入和修改的条件语句整理
     * @param array|string $field 推荐传入数组(一维关联数组，也可以传入二维数组)
     * @param null|string $value
     * @return $this
     */
    public function data($field, $value = null)
    {
        $args = func_get_args();
        if (is_array($field) && self::arrayLevel($field) > 1) {
            $this->payload['insert'] = $field;
            $insert_field = "";
            $insert_value = "";
            foreach ($field as $index => $item) {
                $a2s = self::array2string($item, '');
                $insert_field || ($insert_field = "( {$a2s['k']} )");
                $insert_value .= ", ( {$a2s['v']} ) ";
            }
            $insert_value = trim($insert_value, ',');
            $this->type['insert'] = "$insert_field VALUES $insert_value";
        } elseif (is_array($field) && self::arrayLevel($field) == 1) {
            $data = self::array2string($field, null);
            $this->type['insert'] = $data['insert'];
            $this->type['update'] = $data['update'];
        } else {
            if (!empty($field) && !empty($value)) {
                $this->type['insert'] = "($field)VALUES($value)";
                $this->type['update'] = "$field='$value'";
            } elseif (!empty($field) && empty($value)) {
                $this->type['update'] = $this->type['insert'] = $field;
            } else {
                $this->type['update'] = $this->type['insert'] = "";
            }
        }
        return $this;
    }

    public function select()
    {
        $sql = $this->structure_select();
        return $sql;
    }

    public function find()
    {
        $this->limit(1);
        $sql = $this->structure_select();
        return $sql;
    }

    public function count($field = '*')
    {
        $this->sql_str_exec();
        $this->fields = $this->fields && $this->fields != "*" ? rtrim(trim($this->fields), ",") . "," : "";
        $num = "num_" . time();
        $sql = "SELECT {$this->fields}COUNT($field) AS $num FROM {$this->tableName} {$this->joins} {$this->wheres} {$this->orders} {$this->limits}";
        return $sql;
    }

    /**
     * 获取某个字段的值
     * @param $field string 字段名（单个字段）
     * @return string 返回字段值
     */
    public function value($field = '')
    {
        $this->limit(1);
        $sql = $this->structure_select();
        return $sql;
    }

    public function insert($data = '')
    {
        if (!empty($data)) {
            $this->data($data);
        }
        $sql = self::structure_insert();
        return $sql;
    }

    public function insertAll($data = '')
    {
        if (is_array($data && self::arrayLevel($data) < 2)) {
            die('$data 必须存在且为二维数组');
        } elseif (!is_array($data)) {
            if (!is_array($this->payload['insert']) || (is_array($this->payload['insert']) && self::arrayLevel($this->payload['insert']) < 2)) {
                die('传入的数据不存在或者错误！');
            }
        } else {
            $this->data($data);
        }
        $sql = self::structure_insert();
        $this->payload['insert'] = null;
        return $sql;
    }

    public function update($data = '')
    {
        if (!empty($data)) {
            $this->data($data);
        }
        $sql = self::structure_update();
        return $sql;
    }

    /**
     * 批量更改 单字段 case方法，配合 updateAll使用
     * @param string $field 更改的字段
     * @param string $case_field 条件字段(case后面的)
     * @param string|array $lv2_array 二维数组
     * @return string
     */
    public function update_case($field, $case_field, $lv2_array = '')
    {
        $sql = "$field = CASE $case_field \n";
        if (is_array($lv2_array)) {
            foreach ($lv2_array as $k => $v) {
                $sql .= "WHEN '{$v[$case_field]}' THEN '{$v[$field]}' \n";
            }
        }
        $sql .= "END";
        return $sql;
    }

    /**
     * 批量更改 多字段 case方法
     * @param string|array $data 二维数组
     * @param array $case_fields 条件字段，判断某个修改字段对应的条件字段  [$field => $case_field]
     * @param array $filter_field 过滤字段（不修改的字段）
     * @return bool|int|string
     */
    public function updateAll($data = '', $case_fields = array('id'), $filter_field = array('id'))
    {
        if (is_array($data) && self::arrayLevel($data) > 1) {
            $sql = "";
            foreach ($data as $index => $item) {
                $keys = array_keys($item);
                break;
            }
            foreach ($keys as $k1 => $v1) {
                $field = $v1;
                $case_field = $case_fields[$v1] ?: 'id';
                in_array($v1, $filter_field) || ($sql .= " , " . self::update_case($field, $case_field, $data));
            }
            $sql = trim($sql, ', ');
            $this->type['update'] = $sql;
        } else {
            die('$data 必须存在且为二维数组');
        }
        $sql = self::structure_update();
        return $sql;
    }

    public function delete()
    {
        $sql = self::structure_delete();
        return $sql;
    }


    /**
     * 处理sql语句的拼接
     * @return object
     */
    protected function sql_str_exec()
    {
        $this->orders = empty($this->orders) ? '' : 'ORDER BY ' . rtrim(rtrim($this->orders), ',');
        $this->wheres = empty($this->wheres) ? '' : 'WHERE ' . preg_replace('/(and|or)$/i', '', rtrim($this->wheres));
        $this->groupBys = empty($this->groupBys) ? '' : 'GROUP BY ' . $this->groupBys;
        return $this;
    }

    /**
     * 构建 select 查询语句
     * @return string
     */
    protected function structure_select()
    {
        self::sql_str_exec();
        return $sql = "SELECT {$this->fields} FROM {$this->tableName} {$this->joins} {$this->wheres} {$this->groupBys} {$this->orders} {$this->limits}";
    }

    /**
     * 构建 insert 插入语句
     * @return string
     */
    protected function structure_insert()
    {
        $sql = "INSERT INTO {$this->tableName}{$this->type['insert']}";
        return $sql;
    }

    /**
     * 构建 update 更改语句
     * @return string
     */
    protected function structure_update()
    {
        self::sql_str_exec();
        //去掉首位和末尾的字符","
        $this->type['update'] = empty($this->type['update']) ? '' : trim(trim($this->type['update']), ',');
        $sql = "UPDATE {$this->tableName} SET {$this->type['update']} {$this->wheres} {$this->orders} {$this->limits}";
        return $sql;
    }

    /**
     * 构建 delete 删除语句
     * @return string
     */
    protected function structure_delete()
    {
        self::sql_str_exec();
        $sql = "DELETE FROM {$this->tableName} {$this->wheres} {$this->orders} {$this->limits}";
        return $sql;
    }
}