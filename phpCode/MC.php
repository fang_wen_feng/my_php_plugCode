<?php

namespace app\common\controller\publics;
/**
 * my config
 * Class MC
 * @package app\common\controller\publics
 */
class MC
{
    static private $config = array();
    static private $SE = array();

    static public function set_config($key, $value = null)
    {
        if (is_array($key) && empty($value)) {
            foreach ($key as $k => $v) {
                self::$config[$k] = $v;
            }
        } else {
            self::set_array(self::$config, $key, $value);
        }
    }

    static public function get_config($name)
    {
        return self::get_array(self::$config, $name);
    }

    /**
     * 设置数组, 如: set_array($arr,'lv1.lv2',$value); set_array($arr,array('lv1','lv2'),$value);
     * @param $arr
     * @param string $name
     * @param string $value
     * @param int $index
     * @return mixed
     */
    static public function set_array(&$arr, $name = '', $value = '', $index = 0)
    {
        if ($name && is_array($name)) {
            $len = count($name);
            if ($len > 0 && $index < $len) {
                if (!isset($arr[$name[$index]]) || !is_array($arr[$name[$index]])) {
                    $arr[$name[$index]] = array();
                    if ($index == $len - 1) {
                        $arr[$name[$index]] = $value;
                        return $arr;
                    }
                }
                self::set_array($arr[$name[$index]], $name, $value, $index + 1);
            }
        } elseif ($name && is_string($name) && strpos($name, '.') > 0) {
            $name = explode('.', $name);
            self::set_array($arr, $name, $value, $index);
        } else {
            $arr[$name] = $value;
        }
    }

    /**
     * 获取数组 如： get_array($arr,'lv1.lv2'); get_array($arr,array('lv1','lv2'));
     * @param $arr
     * @param $name
     * @param int $index
     * @return mixed
     */
    static public function get_array(&$arr, $name, $index = 0)
    {
        if ($name && is_array($name)) {
            $len = count($name);
            if ($len > 0 && $index < $len) {
                if ($index == $len - 1) {
                    return $arr[$name[$index]];
                }
                return self::get_array($arr[$name[$index]], $name, (int)$index + 1);
            }
        } elseif ($name && is_string($name) && strpos($name, '.') > 0) {
            $name = explode('.', $name);
            return self::get_array($arr, $name, $index);
        } else {
            return $name ? $arr[$name] : $arr;
        }
    }

    /**
     * 获取缓存文件信息
     * @param string $name
     * @param string $tag
     * @param string $cache_dir
     * @return bool|array|string
     */
    static public function cache_get($name = '', $tag = 'auto_', $cache_dir = '/cache_files')
    {
        $tag = empty($tag) ? 'auto_' : $tag;
        $project_dir = rtrim(str_replace('\\', '/', PROJECT_DIR), '/');
        $cache_file_dir = preg_replace('/[\\\\\/]{2,}/i', '/', $project_dir . '/' . $cache_dir);
        $cache_file_name = md5($tag . $name) . '.txt';
        $cache_file_path = preg_replace('/[\\\\\/]{2,}/i', '/', $cache_file_dir . '/' . $cache_file_name);
        $data = '';
        if (is_file($cache_file_path)) {
            $data = file_get_contents($cache_file_path);
            $data = unserialize($data);
        }
        //文件生成或修改时间
        $mod_time = filemtime($cache_file_path);
        $mod_time = $mod_time ? $mod_time : filectime($cache_file_path);

        if ($data['expire'] > 0) {
            $_t = $mod_time + $data['expire'];
            if ($_t >= time()) {
                return $data['data'];
            } else {
                //过期时间到后就删除文件
                $del_ok = @unlink($cache_file_path);
                return false;
            }
        }
        return $data['data'];
    }

    /**
     * 设置文件缓存
     * @param string $name 名称
     * @param string $value 存入内容
     * @param string $tag 标记
     * @param int $expire 过期时间【小于等于0的时候为永久缓存】
     * @param string $cache_dir 文件缓存目录
     * @return bool|int
     */
    static public function cache_set($name = '', $value = '', $tag = 'auto_', $expire = 0, $cache_dir = '/cache_files')
    {
        $tag = empty($tag) ? 'auto_' : $tag;
        $project_dir = rtrim(str_replace('\\', '/', PROJECT_DIR), '/');
        $cache_file_dir = preg_replace('/[\\\\\/]{2,}/i', '/', $project_dir . '/' . $cache_dir);
        $cache_file_name = md5($tag . $name) . '.txt';
        $cache_file_path = preg_replace('/[\\\\\/]{2,}/i', '/', $cache_file_dir . '/' . $cache_file_name);

        if (!is_dir($cache_file_dir)) {
            $ok = @mkdir($cache_file_dir, 0776, true);
        }
        if ($name !== '' && !is_array($name) && !is_object($name) && $value != '') {
            //过期时间
            $data['expire'] = $expire;
            $data['data'] = $value;
            $data = serialize($data);
            $bit = file_put_contents($cache_file_path, $data, LOCK_EX);
        }
        return $bit;
    }

    /**
     * 删除文件缓存
     * @param string $name 名称
     * @param string $tag 标记
     * @param string $cache_dir 文件缓存目录
     * @return bool
     */
    static public function cache_del($name = '', $tag = 'auto_', $cache_dir = '/cache_files')
    {
        if (is_array($name) || is_object($name)) {
            foreach ($name as $key => $value) {
                self::cache_del($value, $tag, $cache_dir);
            }
        } else {
            if ($name !== '' && !is_bool($name)) {
                $tag = empty($tag) ? 'auto_' : $tag;
                $project_dir = rtrim(str_replace('\\', '/', PROJECT_DIR), '/');
                $cache_file_dir = preg_replace('/[\\\\\/]{2,}/i', '/', $project_dir . '/' . $cache_dir);
                $cache_file_name = md5($tag . $name) . '.txt';
                $cache_file_path = preg_replace('/[\\\\\/]{2,}/i', '/', $cache_file_dir . '/' . $cache_file_name);
                $del_ok = @unlink($cache_file_path);
                return $del_ok;
            }
        }
    }

    /**
     * session 配置
     * param 格式是数组如 session_conf(['k1'=>'x1'],['k2'=>'x2'],...)
     * @return array
     */
    static public function session_conf()
    {
        $args = func_get_args();
        foreach ($args as $index => $arg) {
            if (is_array($arg)) {
                foreach ($arg as $k => $item) {
                    if (!preg_match('/\d+/i', $k)) {
                        self::$SE[$k] = $item;
                    }
                }
            }
        }
        return self::$SE;
    }

    /**
     * session 初始化
     * @param int $t
     * @param string $path
     * @param string $domain
     */
    static public function session_init($t = 0, $path = '', $domain = '')
    {
        //$Lifetime = 3600;
        //$DirectoryPath = "./tmp";
        //is_dir($DirectoryPath) or mkdir($DirectoryPath, 0777);
        //是否开启基于url传递sessionid,这里是不开启，发现开启也要关闭掉
        //if (ini_get("session.use_trans_sid") == true) {
        //    ini_set("url_rewriter.tags", "");
        //    ini_set("session.use_trans_sid", false);
        //}
        if (self::exist(self::$SE['cookie_params'])) {
            if (is_array(self::$SE['cookie_params'])) {
                session_set_cookie_params(self::$SE['cookie_params']);
            } elseif (is_numeric(self::$SE['cookie_params'])) {
                session_set_cookie_params(intval(self::$SE['cookie_params']));
            }
        }
        //session文件存储的路径
        if (self::exist(self::$SE['save_path']) && is_string(self::$SE['save_path'])) {
            is_dir(self::$SE['save_path']) || mkdir(self::$SE['save_path'], 0777, true);
            ini_set('session.save_path', self::$SE['save_path']);
        }
        //session名称
        if (self::exist(self::$SE['name']) && is_string(self::$SE['name'])) {
            ini_set('session.name', self::$SE['name']);
        }
        //设置session生存时间
        if (self::exist(self::$SE['gc_maxlifetime']) && !is_array(self::$SE['gc_maxlifetime']) && !is_object(self::$SE['gc_maxlifetime'])) {
            ini_set('session.gc_maxlifetime', self::$SE['gc_maxlifetime']);
            ini_set("session.gc_divisor", "1");
            ini_set("session.gc_probability", "1");
        }
        //sessionID在cookie中的生存时间
        if (self::exist(self::$SE['cookie_lifetime']) && !is_array(self::$SE['cookie_lifetime']) && !is_object(self::$SE['cookie_lifetime'])) {
            ini_set('session.cookie_lifetime', self::$SE['cookie_lifetime']);
        }
        //sessionID在cookie中的有效路径
        if (self::exist(self::$SE['cookie_path']) && is_string(self::$SE['cookie_path'])) {
            ini_set('session.cookie_path', self::$SE['cookie_path']);
        }
        //sessionID在cookie中的有效域名
        if (self::exist(self::$SE['cookie_domain']) && is_string(self::$SE['cookie_path'])) {
            ini_set('session.cookie_domain', self::$SE['cookie_domain']);
        }
        //sessionID在cookie中的有效协议 1=https
        if (self::exist(self::$SE['cookie_secure']) && is_numeric(self::$SE['cookie_secure'])) {
            ini_set('session.cookie_secure', self::$SE['cookie_secure']);
        }
        //sessionID在cookie中避免被js读取到
        if (self::exist(self::$SE['cookie_httponly'])) {
            ini_set('session.cookie_httponly', 1);
        }

        session_start();
        if (!in_array(true, array(self::exist(self::$SE['cookie_params']), self::exist(self::$SE['cookie_lifetime']), self::exist(self::$SE['name']), self::exist(self::$SE['cookie_path']), self::exist(self::$SE['cookie_domain'])), true)) {
            self::exist($path) || ($path = '');
            self::exist($domain) || ($domain = '');
            $t = self::exist($t) && $t > 0 ? time() + $t : 0;
            self::exist($_COOKIE[session_name()]) || setcookie(session_name(), session_id(), $t, $path, $domain);
        }
        self::exist(self::$config['session_key']) || (self::$config['session_key'] = sha1('session_data'));
    }

    static public function session_key_set($val = null)
    {
        if (self::is_string($val)) {
            self::$config['session_key'] = sha1($val);
        }
        return self::$config['session_key'];
    }

    /**
     * @param string|array $name
     * @param string|* $value
     * @param string $prefix
     */
    static public function session_set($name = '', $value = '', $prefix = '')
    {
        if (is_array($name) && count($name) > 0 && !self::exist($value)) {
            //$name 为数组(一维关联数组时)批量添加
            foreach ($name as $key => $val) {
                $_SESSION[self::$config['session_key']][$prefix . $key] = $val;
            }
        } else {
            if (self::exist($name)) {
                $_SESSION[self::$config['session_key']][$prefix . $name] = $value;
            }
        }
    }

    /**
     * @param string $name
     * @param string $prefix
     * @return mixed
     */
    static public function session_get($name = '', $prefix = '')
    {
        if (self::exist($name)) {
            return $_SESSION[self::$config['session_key']][$prefix . $name];
        } else {
            return $_SESSION[self::$config['session_key']];
        }
    }

    /**
     * @param string|array|null $name
     * @param string|null $prefix
     */
    static public function session_del($name = '', $prefix = '')
    {
        if (is_array($name) && count($name) > 0) {
            //$name 为一维数组时批量删除
            foreach ($name as $item) {
                unset($_SESSION[self::$config['session_key']][$prefix . $item]);
            }
        } elseif (empty($name) && self::is_string($prefix)) {
            // 按前缀批量删除
            foreach ($_SESSION[self::$config['session_key']] as $key => $value) {
                if (strpos(trim($key), $prefix) < 1) {
                    unset($_SESSION[self::$config['session_key']][$key]);
                }
            }
        } else {
            if ($name) {
                unset($_SESSION[self::$config['session_key']][$prefix . $name]);
            }
        }
    }

    /**
     * @param string $path
     * @param string $domain
     * @return string
     */
    static public function session_destroy($path = '/', $domain = '')
    {
        $_SESSION = array();
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), "", time() - 42000, $path, $domain);
        }
        $session_id = session_id();
        session_destroy();
        return $session_id;
    }

    /**
     * 快捷操作 session
     * @param string|array $name
     * @param string|* $value
     * @param string $prefix
     * @param array $conf
     * @return mixed
     */
    static public function session($name = '', $value = '', $prefix = '', $conf = array())
    {
        if (self::is_string($name) && $value === '') {
            return self::session_get($name, $prefix);
        } elseif (self::is_string($name) && self::exist($value)) {
            self::session_set($name, $value, $prefix);
        } elseif (is_array($name) && $value === '') {
            self::session_set($name, '', $prefix);
        } elseif (self::is_string($name) && $value === null) {
            self::session_del($name, $prefix);
        } elseif (is_array($name) && $value === null) {
            self::session_del($name, $prefix);
        } elseif (!self::exist($name) && self::is_string($prefix)) {
            self::session_del($name, $prefix);
        }
        return $_SESSION[self::$config['session_key']];
    }

    static private $CO = array(
        //cookie 名称前缀
        'prefix' => '',
        //cookie 保存时间
        'expire' => 0,
        //cookie 保存路径
        'path' => '',
        //cookie 有效域名
        'domain' => '',
        //cookie 启用安全传输
        'secure' => false,
        //cookie 使用httponly设置，避免被js读取到
        'httponly' => false,
        //是否启用这个配置
        'is_enable' => false,
    );

    /**
     * cookie 配置
     * @param array $config
     * @param boolean $is_enable 是否启用配置
     * @return array
     */
    static public function cookie_init(array $config = array(), $is_enable = true)
    {
        self::$CO = array_merge(self::$CO, array_change_key_case($config));
        ($is_enable === true) && (self::$CO['is_enable'] = true);
        return self::$CO;
    }

    /**
     * 自定义设置cookie
     * @param string $name 名称
     * @param string $value 值
     * @param int $expire 过期时间
     * @param string $path 路径
     * @param string $domain 域名
     * @param bool $secure 是否安全链接(https)
     * @param bool $httponly 是否被浏览器js代码读取到
     * @param string $samesite [None,Lax,Strict]
     * @return string
     */
    static final public function setcookie($name = '', $value = '', $expire = 0, $path = '', $domain = '', $secure = false, $httponly = false, $samesite = '')
    {
        $name = urlencode($name);
        $value = urlencode($value);
        $str[] = "$name=$value";
        if (is_numeric($expire) && !in_array($expire, [0, '0'], true)) {
            $str[] = "expires=" . gmstrftime("%A, %d-%b-%Y %H:%M:%S GMT", time() + intval($expire));
            $expire > 0 && ($str[] = "Max-Age=$expire");
        }
        self::is_string($path) && ($str[] = "path=$path");
        self::is_string($domain) && ($str[] = "domain=$domain");
        $httponly && ($str[] = "HttpOnly");
        self::is_string($samesite) && ($str[] = "SameSite=$samesite");
        $secure && ($str[] = "Secure");
        $cookie_str = "Set-Cookie: " . join('; ', $str);
        if (self::is_string($samesite)) {
            header($cookie_str, false);
        } else {
            setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
        }
        return $cookie_str;
    }

    /**
     * 设置 cookie
     * @param string|array $name
     * @param string $value
     * @param string $prefix 设置cookie名称前缀
     * @param int|array $expire 时间在$expire秒后到期
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * @param string $samesite
     */
    static public function cookie_set($name = '', $value = '', $prefix = '', $expire = 0, $path = '/', $domain = '', $secure = false, $httponly = false, $samesite = '')
    {
        if (self::$CO['is_enable'] === true) {
            $prefix = self::is_string(self::$CO['prefix']) ? self::$CO['prefix'] : $prefix;
            $expire = self::is_string(self::$CO['expire']) ? self::$CO['expire'] : $expire;
            $path = self::is_string(self::$CO['path']) ? self::$CO['path'] : $path;
            $domain = self::is_string(self::$CO['domain']) ? self::$CO['domain'] : $domain;
            $secure = self::exist(self::$CO['secure']);
            $httponly = self::exist(self::$CO['httponly']);
        } else {
            if (is_array($expire)) {
                $conf = $expire;
                $expire = self::exist($conf['expire']) ? $conf['expire'] : 0;
                $path = self::exist($conf['path']) ? $conf['path'] : '';
                $domain = self::exist($conf['domain']) ? $conf['domain'] : '';
                $secure = self::exist($conf['secure']) ? $conf['secure'] : false;
                $httponly = self::exist($conf['httponly']) ? $conf['httponly'] : false;
            }
        }
        $expire = $expire > 0 ? time() + (int)$expire : 0;
        if (self::is_string($name) && self::is_string($value)) {
            self::setcookie($prefix . $name, $value, $expire, $path, $domain, $secure, $httponly, $samesite);
        } elseif (is_array($name) && count($name) > 0) {
            foreach ($name as $key => $item) {
                if (self::is_string($key) && self::is_string($item)) {
                    self::setcookie($prefix . $key, $item, $expire, $path, $domain, $secure, $httponly, $samesite);
                }
            }
        }
    }

    /**
     * 获取cookie值
     * @param string|array $name
     * @param string $prefix cookie前缀
     * @return array|bool|mixed
     */
    static public function cookie_get($name = '', $prefix = '')
    {
        if (!is_array($_COOKIE)) {
            return false;
        }
        $data = array();
        if (self::is_string($name)) {
            return $_COOKIE[$prefix . $name];
        } elseif (is_array($name) && count($name) > 0) {
            foreach ($name as $key) {
                is_string($key) && ($data[$prefix . $key] = $_COOKIE[$prefix . $key]);
            }
            return $data;
        } elseif (!self::exist($name) && self::is_string($prefix)) {
            //根据后缀批量获取
            foreach ($_COOKIE as $key => $item) {
                preg_match("/^$prefix/i", $key) && ($data[$key] = $item);
            }
            return $data;
        }
        return $_COOKIE;
    }

    /**
     * 删除cookie
     * @param string|array $name cookie名称
     * @param string $prefix cookie前缀
     * @param string|array $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     */
    static public function cookie_del($name = '', $prefix = '', $path = '/', $domain = '', $secure = false, $httponly = false)
    {
        if (self::$CO['is_enable'] === true) {
            $prefix = self::is_string(self::$CO['prefix']) ? self::$CO['prefix'] : $prefix;
            $expire = self::is_string(self::$CO['expire']) ? self::$CO['expire'] : 0;
            $path = self::is_string(self::$CO['path']) ? self::$CO['path'] : $path;
            $domain = self::is_string(self::$CO['domain']) ? self::$CO['domain'] : $domain;
            $secure = self::exist(self::$CO['secure']);
            $httponly = self::exist(self::$CO['httponly']);
        } else {
            if (is_array($path)) {
                $conf = $path;
                $path = self::exist($conf['path']) ? $conf['path'] : '';
                $domain = self::exist($conf['domain']) ? $conf['domain'] : '';
                $secure = self::exist($conf['secure']) ? $conf['secure'] : false;
                $httponly = self::exist($conf['httponly']) ? $conf['httponly'] : false;
            }
        }
        $expire = time() - 1000;
        if (self::is_string($name)) {
            setcookie($prefix . $name, '', $expire, $path, $domain, $secure, $httponly);
        } elseif (!self::exist($name) && self::is_string($prefix)) {
            //根据前缀进行删除
            foreach ($_COOKIE as $key => $item) {
                preg_match("/^$prefix/i", $key) && setcookie($key, '', $expire, $path, $domain, $secure, $httponly);
            }
        } elseif (is_array($name) && count($name) > 0) {
            foreach ($name as $key) {
                is_string($key) && setcookie($prefix . $key, '', $expire, $path, $domain, $secure, $httponly);
            }
        }
    }

    /**
     * 快捷操作cookie
     * @param string|array|null $name
     * @param string|null $value
     * @param string $prefix
     * @param int|array $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * @param string $samesite
     * @return array|bool|mixed
     */
    static public function cookie($name = '', $value = '', $prefix = '', $expire = 0, $path = '/', $domain = '', $secure = false, $httponly = false, $samesite = '')
    {
        if (self::$CO['is_enable'] === true) {
            $prefix = self::is_string(self::$CO['prefix']) ? self::$CO['prefix'] : $prefix;
            $expire = self::is_string(self::$CO['expire']) ? self::$CO['expire'] : $expire;
            $path = self::is_string(self::$CO['path']) ? self::$CO['path'] : $path;
            $domain = self::is_string(self::$CO['domain']) ? self::$CO['domain'] : $domain;
            $secure = self::exist(self::$CO['secure']);
            $httponly = self::exist(self::$CO['httponly']);
        } else {
            if (is_array($expire)) {
                $conf = $expire;
                $expire = self::exist($conf['expire']) ? $conf['expire'] : 0;
                $path = self::exist($conf['path']) ? $conf['path'] : '';
                $domain = self::exist($conf['domain']) ? $conf['domain'] : '';
                $secure = self::exist($conf['secure']) ? $conf['secure'] : false;
                $httponly = self::exist($conf['httponly']) ? $conf['httponly'] : false;
            }
        }
        if (self::is_string($name) && $value === '') {
            return self::cookie_get($name, $prefix);
        } elseif (self::is_string($name) && self::is_string($value)) {
            self::cookie_set($name, $value, $prefix, $expire, $path, $domain, $secure, $httponly, $samesite);
        } elseif (self::is_string($name) && $value === null) {
            self::cookie_del($name, $prefix, $path, $domain, $secure, $httponly);
        } elseif (is_array($name) && $value === '') {
            self::cookie_set($name, $value, $prefix, $expire, $path, $domain, $secure, $httponly, $samesite);
        } elseif (is_array($name) && $value === null) {
            self::cookie_del($name, $prefix, $path, $domain, $secure, $httponly);
        } elseif (!self::exist($name) && self::is_string($prefix)) {
            self::cookie_del($name, $prefix, $path, $domain, $secure, $httponly);
        }
        return $_COOKIE;
    }

    static private $TK = array(
        //多少秒后过期
        'exp' => 3600,
        //多少秒后生效
        'nbf' => 0,
        //签发时间
        'iat' => 0,
        //面向用户，如在哪个域名下可用等
        'sub' => null,
        //签名加密密匙
        'sign_key' => 'kuqfXfT15nqSI7ZQEUTbsQ9L8',
        //加密函数名称
        'func' => 'sha384',
    );

    /**
     * token 配置
     * param 格式是数组如 token_conf(['k1'=>'x1'],['k2'=>'x2'],...)
     * @return array
     */
    static public function token_conf()
    {
        $args = func_get_args();
        foreach ($args as $index => $arg) {
            if (is_array($arg)) {
                foreach ($arg as $k => $item) {
                    if (!preg_match('/\d+/i', $k)) {
                        self::$TK[$k] = $item;
                    }
                }
            }
        }
        return self::$TK;
    }

    /**
     * 新建token
     * @param array $payload 一维数组
     * @param int $exp 过期时间
     * @param int $nbf 生效时间
     * @param null|string $sign_key 加密密匙
     * @return string
     */
    static public function create_token($payload = array(), $exp = 3600, $nbf = 0, $sign_key = null)
    {
        is_array($payload) || ($payload = array());
        $func = self::$TK['func'];
        $t = time();
        self::is_string($sign_key) || ($sign_key = self::$TK['sign_key']);
        $exp = is_numeric($exp) ? $t + intval($exp) : $t + 3600;
        $nbf = is_numeric($nbf) ? $t + intval($nbf) : $t;
        $sub = self::$TK['sub'];
        $payload['exp'] = $exp;
        $payload['nbf'] = $nbf;
        $payload['iat'] = $t;
        self::exist($sub) && ($payload['sub'] = $sub);
        ksort($payload);
        $segments = array();
        //第一部分，载荷部分 payload
        $segments[] = self::encode64(json_encode($payload));
        //第二部分，签名加密部分
        $segments[] = self::encode64(self::signature($segments[0], $sign_key, $func));
        /*//再进行base64编码
        return self::encode64(join('.', $segments));*/
        return join('.', $segments);
    }

    /**
     * 验证token
     * @param string $token
     * @param null|string $sign_key
     * @param null|callable $fn
     * @param null|callable $fn1
     * @return bool|mixed
     */
    static public function check_token($token, $sign_key = null, $fn = null, $fn1 = null)
    {
        $t = time();
        $func = self::$TK['func'];
        $msg = array();
        self::is_string($sign_key) || ($sign_key = self::$TK['sign_key']);
        /*//先进行base64解码
        $token = explode('.', self::decode64($token));*/
        $token = explode('.', $token);
        //载荷部分数据
        $payload = json_decode(self::decode64($token[0]), true);
        //签名
        $sign = self::decode64($token[1]);
        //验证token格式是否正确
        if (count($token) !== 2) {
            $msg = array('msg' => 'token格式错误', 'value' => false);
            is_callable($fn) && $fn($payload, $msg);
            return false;
        }
        //验证签名是否正确
        if ($sign !== self::signature($token[0], $sign_key, $func)) {
            $msg = array('msg' => '验证签名错误', 'value' => false);
            is_callable($fn) && $fn($payload, $msg);
            return false;
        }
        //验证是否过期
        if ($t > $payload['exp']) {
            $msg = array('msg' => 'token过期，token的到期时间为：' . date('Y-m-d H:i:s', $payload['exp']), 'value' => false);
            is_callable($fn) && $fn($payload, $msg);
            return false;
        }
        //验证是否到生效时间
        if ($t < $payload['nbf']) {
            $msg = array('msg' => 'token未到生效期，token的生效时间为：' . date('Y-m-d H:i:s', $payload['nbf']), 'value' => false);
            is_callable($fn) && $fn($payload, $msg);
            return false;
        }
        //自定义验证操作
        if (is_callable($fn1)) {
            return $fn1($payload, $sign, $token);
        }
        //验证ip地址
        if (self::exist($payload['ip'])) {
            if (md5(self::ip()) !== $payload['ip']) {
                $msg = array('msg' => 'ip地址验证错误', 'value' => false);
                is_callable($fn) && $fn($payload, $msg);
                return false;
            }
        }
        //验证客户端信息
        if (self::exist($payload['user_agent'])) {
            if (md5($_SERVER['HTTP_USER_AGENT']) !== $payload['user_agent']) {
                $msg = array('msg' => '客户端信息验证错误', 'value' => false);
                is_callable($fn) && $fn($payload, $msg);
                return false;
            }
        }
        return $payload;
    }

    /**
     * 签名加密
     * @param string $string
     * @param string $key 加密密匙
     * @param string $func 加密函数名称
     */
    static public function signature($string, $key = null, $func = 'md5')
    {
        self::is_string($key) || ($key = self::$TK['sign_key']);
        $str = $func . $key . $string . $key;
        $str = self::sha256($str) . $str . $key;
        if (function_exists($func)) {
            return $func($str);
        } else {
            return self::sha256($str);
        }
    }

    static public function create_login_token($payload = array(), $exp = 3600, $nbf = 0)
    {
        $payload['ip'] = md5(self::ip());
        $payload['user_agent'] = md5($_SERVER['HTTP_USER_AGENT']);
        return self::create_token($payload, $exp, $nbf);
    }

    static public function check_login_token($token = '', $fn = null, $fn1 = null)
    {
        $payload = self::check_token($token, null, $fn, $fn1);
        if($payload){
            //检测id是否存在
            if (trim($payload['id']) === '') {
                return false;
            }
            //检测name是否存在
            if (trim($payload['name']) === '') {
                return false;
            }
            //检测is_admin是否存在
            if (trim($payload['is_admin']) === '') {
                return false;
            }
        }else{
            return false;
        }
        return $payload;
    }

    /**
     * 判断是否存在
     * @param $var
     * @param array $opt 这里的值代表了不存在的意思
     * @return bool
     */
    static public function exist($var, $opt = array("", null, false))
    {
        isset($var) || ($var = false);
        if (in_array($var, $opt, true)) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否是字符串
     * @param $var
     * @return bool
     */
    static public function is_string($var)
    {
        return !in_array($var, array('', null, false), true) && (is_string($var) || is_numeric($var));
    }

    /**
     * 浏览器友好的变量输出
     * @param mixed $var 变量
     * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
     * @param string $label 标签 默认为空
     * @param boolean $strict 是否严谨 默认为true
     * @return void|string
     */
    static public function dump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            } else {
                $output = $label . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else
            return $output;
    }

    /**
     * 获取存在的值
     * @return bool
     */
    static public function vars()
    {
        $var = false;
        $args = func_get_args();
        foreach ($args as $arg) {
            if (self::exist($arg)) {
                $var = $arg;
                break;
            }
        }
        return $var;
    }

    static public function encode64($data){
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    static public function decode64($data){
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * @param int $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @return mixed
     */
    static public function ip($type = 0){
        $type = $type ? 1 : 0;
        static $ip = NULL;
        if ($ip !== NULL) return $ip[$type];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos) unset($arr[$pos]);
            $ip = trim($arr[0]);
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u", ip2long($ip));
        $ip = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }

    /**
     * 字符串截取和返回字符串的长度
     * @param string $str 要截取的字符串
     * @param int $start 字符串截取的初始位置，从0开始
     * @param int $length 字符串截取的长度
     * @param string $charset 字符串编码
     * @param bool $suffix 是否添加后缀
     * @param bool $strlen 是否返回字符串的长度(false不返回,true返回)
     * @return int|string
     */
    static final public function my_substr($str, $start = 0, $length, $charset = 'utf-8', $suffix = true, $strlen = false)
    {
        $charset || ($charset = 'utf-8');
        //正则表达式匹配编码
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        //返回字符串长度
        if ($strlen) {
            if (function_exists('mb_strlen')) {
                $count = mb_strlen($str, $charset);
            } elseif (function_exists('iconv_strlen')) {
                $count = iconv_strlen($str, $charset);
            } else {
                preg_match_all($re[$charset], $str, $match);
                $count = count($match[0]);
            }
            return $count;
        }
        //截取字符串
        if (function_exists("mb_substr"))
            $slice = mb_substr($str, $start, $length, $charset);
        elseif (function_exists('iconv_substr')) {
            $slice = iconv_substr($str, $start, $length, $charset);
            if (false === $slice) {
                $slice = '';
            }
        } else {
            preg_match_all($re[$charset], $str, $match);
            $slice = join("", array_slice($match[0], $start, $length));
        }
        //字数不满添加后缀 ...
        if ($suffix) {
            $count = self::my_substr($str, $start, $length, $charset, false, true);
            if ($count > $length) {
                return $slice . '......';
            } else {
                return $slice;
            }
        } else {
            return $slice;
        }
    }

    /**
     * 返回字符串长度
     * @param string $str
     * @param string $charset
     * @return int
     */
    static final public function my_strlen($str, $charset = 'utf-8')
    {
        $charset || ($charset = 'utf-8');
        //正则表达式匹配编码
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";

        //返回字符串长度
        if (function_exists('mb_strlen')) {
            $count = mb_strlen($str, $charset);
        } elseif (function_exists('iconv_strlen')) {
            $count = iconv_strlen($str, $charset);
        } else {
            preg_match_all($re[$charset], $str, $match);
            $count = count($match[0]);
        }
        return $count;
    }

    /**
     * 字符串分隔
     * @param string $str
     * @param int $split_length
     * @param string $charset
     * @return array|array[]|bool|false|string[]
     */
    static final public function my_str_split($str, $split_length = 1, $charset = 'utf-8')
    {
        if (func_num_args() == 1 && strtolower($charset) === 'utf-8') {
            return preg_split('/(?<!^)(?!$)/u', $str);
        }
        if ($split_length < 1) {
            return false;
        }
        $len = self::my_strlen($str, $charset);
        $arr = array();
        for ($i = 0; $i < $len; $i += $split_length) {
            $s = self::my_substr($str, $i, $split_length, $charset, false);
            $arr[] = $s;
        }
        return $arr;
    }

    /**
     * 把搜索词分割成多段并存储（存在 $save_arr 参数里面），左->右，字数由多到少，$save_arr 最终是一个二维数组
     * @param int $length 截取的长度
     * @param array $array 一维数组
     * @param $save_arr
     */
    static public function search_search1($length, $array, &$save_arr){
        for ($i = 0, $len = count($array); $i < $len; $i++) {
            ($i + $length <= $len) && ($save_arr[] = array_slice($array, $i, $length));
        }
    }

    /**
     * 搜索词分割
     * @param string $keyword 搜索词
     * @param int|boolean $toStirng [布尔值或者 0，1]
     * @return bool|array
     */
    static public function search_keywords($keyword, $toStirng = 0){
        if(!$keyword){return false;}
        $array = self::my_str_split($keyword);
        $len = count($array);
        for($i = $len;$i>0;$i--){
            self::search_search1($i,$array,$save_arr);
        }
        if($toStirng){
            for($i=0,$len=count($save_arr);$i<$len;$i++){
                $save_arr[$i] = join('',$save_arr[$i]);
            }
        }
        return $save_arr ? $save_arr : false;
    }

    /**
     * 最小长度搜索，返回搜索条件字符串(regexp方式的)
     * @param string $keywords
     * @param int $min_len 搜索词的最小长度
     * @param null|callable $fn 回调函数，传入4个参数
     * @return array|bool|string
     */
    static public function search_min_len_keyword($keywords = '', $min_len = 2, $fn = null)
    {
        if ($keywords) {
            $k_len = self::my_strlen($keywords);
            //最小长度搜索词
            ($min_len == 0) && ($min_len = '');
            if (!preg_match('/^\d{1,2}$/i', $min_len)) {
                in_array($k_len, [1, 2], true) && ($min_len = 1);
                in_array($k_len, [3, 4], true) && ($min_len = 2);
                ($k_len > 4) && ($min_len = $k_len - 3);
            }
            (intval($min_len) > $k_len) && ($min_len = $k_len);
            //分割搜索词（一维数组），处理分割词长度
            $reg = self::search_keywords($keywords, 1);
            foreach ($reg as $index => $item) {
                //去除小于最小长度的搜索词
                if (self::my_strlen($item) < intval($min_len)) {
                    unset($reg[$index]);
                }
            }
            //过滤非法字符
            $reg = is_array($reg) && count($reg) > 0 ? '(' . preg_replace('/(\(|\)|\[|\]|\{|\}|\^|\$|\.|\*|\+|\?|\/|\\\\\\\\)/i', '\\\\\\\\$1', join('|', $reg)) . ')' : false;
            is_callable($fn) && $fn($keywords, $min_len, $reg, $k_len);
            return $reg;
        }
        return false;
    }

    static public function sha224($data = '', $raw_output = false){return hash('sha224', $data, $raw_output);}
    static public function sha256($data = '', $raw_output = false){return hash('sha256', $data, $raw_output);}
    static public function sha384($data = '', $raw_output = false){return hash('sha384', $data, $raw_output);}
    static public function sha512($data = '', $raw_output = false){return hash('sha512', $data, $raw_output);}
    static public function sha3_224($data = '', $raw_output = false){return hash('sha3-224', $data, $raw_output);}
    static public function sha3_256($data = '', $raw_output = false){return hash('sha3-256', $data, $raw_output);}
    static public function sha3_384($data = '', $raw_output = false){return hash('sha3-384', $data, $raw_output);}
    static public function sha3_512($data = '', $raw_output = false){return hash('sha3-512', $data, $raw_output);}
    /**
     * 密码字符加密 crypt CRYPT_SHA512
     * @param string $str 加密字符串
     * @param string $salt 盐值，最大长度是16，中英文都可以，不过要注意一个中文字符占3个字节(utf-8)
     * @param int $rounds 循环次数，最小是 1000，最大是 999999999
     * @return string
     */
    static public function pass_sha512($str = '', $salt = '', $rounds = 6000)
    {
        $salt = substr($salt, 0, 16);
        ($rounds < 1000) && ($rounds = 1000);
        ($rounds > 999999999) && ($rounds = 999999999);
        return crypt($str, '$6$rounds=' . $rounds . '$' . $salt);
    }

    /**
     * 密码字符加密 crypt CRYPT_SHA256
     * @param string $str 加密字符串
     * @param string $salt 盐值，最大长度是16，中英文都可以，不过要注意一个中文字符占3个字节(utf-8)
     * @param int $rounds 循环次数，最小是 1000，最大是 999999999
     * @return string
     */
    static public function pass_sha256($str = '', $salt = '', $rounds = 6000)
    {
        $salt = substr($salt, 0, 16);
        ($rounds < 1000) && ($rounds = 1000);
        ($rounds > 999999999) && ($rounds = 999999999);
        return crypt($str, '$5$rounds=' . $rounds . '$' . $salt);
    }

    /**
     * 密码字符加密 crypt CRYPT_BLOWFISH，(>=PHP 5.3.7)
     * @param string $str 加密字符串
     * @param string $salt 盐值，长度是22，字符串只能是 [./0-9A-Za-z]
     * @param int $rounds 循环次数，04-31
     * @return string
     */
    static public function pass_blowfish($str = '', $salt = '', $rounds = 11)
    {
        ($rounds < 4) && ($rounds = '04');
        ($rounds > 31) && ($rounds = '31');
        $rounds = substr('0' . $rounds, -2);
        $salt = preg_replace('/[^\.\/0-9A-Za-z]/', '', $salt);
        (strlen($salt) < 22) && ($salt = str_pad($salt, 22, '.'));
        return crypt($str, '$2y$' . $rounds . '$' . $salt);
    }

    /**
     * 密码字符加密 crypt CRYPT_EXT_DES
     * @param string $str 加密字符串
     * @param string $salt 盐值，长度是4，字符串只能是 [./0-9A-Za-z]
     * @param string $rounds 循环次数，长度是4，字符串只能是 [./0-9A-Za-z]
     * @return string
     */
    static public function pass_ext_des($str = '', $salt = '', $rounds = 'J1'){
        $rounds = ltrim($rounds . '', '0');
        $rounds = preg_replace('/[^\.\/0-9A-Za-z]/', '', $rounds . '');
        $rounds = substr($rounds, 0, 4);
        (strlen($rounds) !== 4) && ($rounds = str_pad($rounds, 4, '.'));
        $salt = preg_replace('/[^\.\/0-9A-Za-z]/', '', $salt);
        $salt = substr($salt, 0, 4);
        (strlen($salt) !== 4) && ($salt = str_pad($salt, 4, '.'));
        return crypt($str, '_' . $rounds . $salt);
    }

    /**
     * 密码字符加密 crypt CRYPT_STD_DES
     * @param string $str 加密字符串
     * @param string $salt 盐值，长度是2，字符串只能是 [./0-9A-Za-z]
     * @return string
     */
    static public function pass_std_des($str = '', $salt = '')
    {
        $salt = preg_replace('/[^\.\/0-9A-Za-z]/', '', $salt);
        $salt = substr($salt, 0, 2);
        (strlen($salt) !== 2) && ($salt = str_pad($salt, 2, '.'));
        return crypt($str, $salt);
    }

    /**
     * 密码字符加密 crypt CRYPT_MD5
     * @param string $str 加密字符串
     * @param string $salt 盐值，最大长度是8，超过部分还是取前面8个字符为盐值
     * @return string
     */
    static public function pass_md5($str = '', $salt = '')
    {
        $salt = substr($salt, 0, 8);
        return crypt($str, '$1$' . $salt);
    }

    static public function my_hash($str, $salt, $algo_fn = 'sha1')
    {
        $str = $algo_fn($str);
        $salt = $algo_fn($salt);
        $len = ~~(strlen($salt) / 4);
        $str_arr = str_split($str, $len);
        $salt_arr = str_split($salt, $len);
        $arr = array($salt_arr[2], $str_arr[0], $str_arr[3], $salt_arr[0], $str_arr[1], $salt_arr[3], $salt_arr[1], $str_arr[2]);
        return join('', $arr);
    }

    /**
     * 页面跳转
     * @param string $url 页面链接
     * @param int $code 状态码
     */
    static final public function url($url = '', $code = 302)
    {
        if (self::is_string($url)) {
            preg_match('/^\//i', $url) && ($url = self::get_url() . $url);
            header('Location: ' . $url, true, $code);
            exit();
        }
    }

    static final public function get_url($name = '')
    {
        $data = [];
        $scheme = self::is_ssl() ? 'https://' : 'http://';
        $uri = $_SERVER['PHP_SELF'] . $_SERVER['QUERY_STRING'];
        if ($_SERVER['REQUEST_URI']) {
            $uri = $_SERVER['REQUEST_URI'];
        }
        //首页链接
        $data['host'] = $scheme . $_SERVER['HTTP_HOST'];
        //当前链接
        $data['url'] = $scheme . $_SERVER['HTTP_HOST'] . $uri;
        return $name ? $data[$name] : $data['host'];
    }

    /**
     * 判断是否SSL协议
     * @return boolean
     */
    static final private function is_ssl()
    {
        if (isset($_SERVER['HTTPS']) && ('1' == $_SERVER['HTTPS'] || 'on' == strtolower($_SERVER['HTTPS']))) {
            return true;
        } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
            return true;
        }
        return false;
    }

    /**
     * 参数字符串构造
     * @param array $arr 一维、二维数组 $arr = ['key'=>'key1',['k1'=>1,'k2'=>1],['k1'=>2,'k2'=>2],...];
     * @return mixed
     */
    static final public function structure_param($arr = [], $op = 'param')
    {
        $data['param'] = '';
        $data['get'] = '';
        foreach ($arr as $index => $item) {
            if (!is_num($index)) {
                $data['param'] .= "$index/$item/";
                $data['get'] .= "$index=$item&";
            } else {
                if (is_array($item)) {
                    foreach ($item as $k => $v) {
                        $data['param'] .= "{$k}[]/$v/";
                        $data['get'] .= "{$k}[]=$v&";
                    }
                }
            }
        }
        $data['param'] = trim($data['param'], '/');
        $data['get'] = trim($data['get'], '&');
        return self::is_string($op) ? $data[$op] : $data;
    }

    static final public function is_post(){return strtolower($_SERVER["REQUEST_METHOD"]) == 'post';}
    static final public function is_get(){return strtolower($_SERVER["REQUEST_METHOD"]) == 'get';}
    static final public function is_ajax(){return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';}

    static function __callStatic($name, $arguments)
    {
        // TODO: Implement __callStatic() method.
        $className = self::class;
        self::dump("方法 $className::$name(" . join(', ', $arguments) . ") 不可访问或不存在");
    }
}