<?php
class Rsa
{
    //加密切割长度
    const  RSA_ENCRYPT_BLOCK_SIZE = 117;
    //解密切割长度
    const  RSA_DECRYPT_BLOCK_SIZE = 128;

    /**
     * @param string $private_key 私钥
     * @return false|resource
     */
    public static function getPrivateKey($private_key = '')
    {
        return openssl_pkey_get_private($private_key);
        //return $private_key ?: false;
    }

    /**
     * @param string $public_key 公钥
     * @return false|resource
     */
    public static function getPublicKey($public_key = '')
    {
        return openssl_pkey_get_public($public_key);
        //return $public_key ?: false;
    }

    /**
     * 私钥加密
     * @param string $private_key 私钥字符
     * @param string $content 加密字符串
     * @return string|null
     */
    public static function privateEncrypt($private_key, $content = '')
    {
        if (!is_string($content)) {
            return null;
        }
        $result = '';
        $data = str_split($content, self::RSA_ENCRYPT_BLOCK_SIZE);
        foreach ($data as $block) {
            openssl_private_encrypt($block, $dataEncrypt, self::getPrivateKey($private_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataEncrypt;
        }
        return $result ? base64_encode($result) : null;
    }

    /**
     * 公钥加密
     * @param string $public_key 公钥字符
     * @param string $content 加密字符串
     * @return string|null
     */
    public static function publicEncrypt($public_key, $content = '')
    {
        if (!is_string($content)) {
            return null;
        }
        $result = '';
        $data = str_split($content, self::RSA_ENCRYPT_BLOCK_SIZE);
        foreach ($data as $block) {
            openssl_public_encrypt($block, $dataEncrypt, self::getPublicKey($public_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataEncrypt;
        }
        return $result ? base64_encode($result) : null;
    }

    /**
     * 私钥解密
     * @param string $private_key 私钥
     * @param string $encrypted 解密字符串
     * @return string|null
     */
    public static function privateDecrypt($private_key, $encrypted = '')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $result = '';
        $data = str_split(base64_decode($encrypted), self::RSA_DECRYPT_BLOCK_SIZE);
        foreach ($data as $block) {
            openssl_private_decrypt($block, $dataDecrypt, self::getPrivateKey($private_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataDecrypt;
        }
        return $result ? $result : null;
    }

    /**
     * 公钥解密
     * @param string $public_key 公钥
     * @param string $encrypted 解密字符串
     * @return string|null
     */
    public static function publicDecrypt($public_key, $encrypted = '')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $result = '';
        $data = str_split(base64_decode($encrypted), self::RSA_DECRYPT_BLOCK_SIZE);
        foreach ($data as $block) {
            openssl_public_decrypt($block, $dataDecrypt, self::getPublicKey($public_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataDecrypt;
        }
        return $result ? $result : null;
    }


    /**
     * 私钥加签
     * @param string $data 待签名字符串
     * @param string $private_key 私钥
     * @param string|int $algorithm 签名算法如 sha256
     * @return null|string
     */
    public static function sign($data, $private_key = '', $algorithm = OPENSSL_ALGO_SHA256)
    {
        if (!self::is_string($data)) {
            return null;
        }
        $private_key_id = openssl_pkey_get_private($private_key);
        openssl_sign($data, $sign, $private_key_id, $algorithm);
        openssl_free_key($private_key_id);
        $sign = self::encode64($sign);
        return $sign;
    }

    /**
     * 公钥验签
     * @param string $data 待签名字符串
     * @param string $sign 签名
     * @param string $public_key 公钥
     * @param int|string $algorithm 签名算法如 sha256
     * @return bool|null
     */
    public static function verify($data, $sign, $public_key = '', $algorithm = OPENSSL_ALGO_SHA256)
    {
        if (!self::is_string($data)) {
            return null;
        }
        $public_key_id = openssl_pkey_get_public($public_key);
        $ok = openssl_verify($data, self::decode64($sign), $public_key_id, $algorithm);
        openssl_free_key($public_key_id);
        return $ok === 1 ? true : false;
    }

    /**
     * base64编码
     * @param $data
     * @return string
     */
    public static function encode64($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * base64解码
     * @param $data
     * @return bool|string
     */
    public static function decode64($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * 判断是否存在
     * @param $var
     * @param array $opt 这里的值代表了不存在的意思
     * @return bool
     */
    public static function is_exist($var, $opt = array("", null, false))
    {
        isset($var) || ($var = false);
        if (in_array($var, $opt, true)) {
            return false;
        }
        return true;
    }

    /**
     * 判断是否是字符串
     * @param $var
     * @return bool
     */
    public static function is_string($var)
    {
        if (self::is_exist($var) && (is_string($var) || is_numeric($var) || is_int($var) || is_float($var))) {
            return true;
        }
        return false;
    }

    /**
     * 获取变量值
     * @return bool|*
     */
    public static function get_vars()
    {
        $args = func_get_args();
        $var = false;
        foreach ($args as $arg) {
            if (is_exist($arg)) {
                $var = $arg;
                break;
            }
        }
        return $var;
    }
}
