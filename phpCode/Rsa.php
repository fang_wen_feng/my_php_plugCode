<?php
/**
 * Created by PhpStorm.
 * User: fang
 * Date: 2023/8/31
 * Time: 14:24
 */

class Rsa
{
    //RSA密钥长度
    private $Bit = 1024;
    //加密切割长度，即 ($Bit/8-11)，【PKCS1建议的padding就占用了11个字节】
    private $rsa_encrypt_block_size = 117;
    //解密切割长度，即 ($Bit/8)
    private $rsa_decrypt_block_size = 128;

    public static function init($bit = 1024)
    {
        $rsa = new Rsa();
        return $rsa->set_block_size($bit);
    }

    /**
     * 计算处理 加密切割长度、解密切割长度
     * @param int $bit 密钥长度
     * @return $this
     */
    public function set_block_size($bit = 1024)
    {
        if (self::is_string($bit)) {
            $bit = (int)$bit;
            ($bit < 1024) && ($bit = 1024);
            $this->rsa_encrypt_block_size = ~~($bit / 8) - 11;
            $this->rsa_decrypt_block_size = ~~($bit / 8);
            $this->Bit = $this->rsa_decrypt_block_size * 8;
        }
        return $this;
    }

    /**
     * 获取私钥
     * @param string $private_key
     * @return bool|resource
     */
    public static function getPrivateKey($private_key = '')
    {
        return openssl_pkey_get_private($private_key);
        //return $private_key ?: false;
    }

    /**
     * 获取公钥
     * @param string $public_key
     * @return resource
     */
    public static function getPublicKey($public_key = '')
    {
        return openssl_pkey_get_public($public_key);
        //return $public_key ?: false;
    }

    /**
     * 私钥加密
     * @param string $private_key 私钥字符
     * @param string $content 加密字符串
     * @param boolean $is_url 是否用于地址链接传递
     * @return string|null
     */
    public function privateEncrypt($private_key, $content = '', $is_url = false)
    {
        if (!is_string($content)) {
            return null;
        }
        $result = '';
        $data = str_split($content, $this->rsa_encrypt_block_size);
        foreach ($data as $block) {
            openssl_private_encrypt($block, $dataEncrypt, self::getPrivateKey($private_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataEncrypt;
        }
        if ($is_url) {
            return $result ? self::encode64($result) : null;
        }
        return $result ? base64_encode($result) : null;
    }

    /**
     * 公钥加密
     * @param string $public_key 公钥字符
     * @param string $content 加密字符串
     * @param boolean $is_url 是否用于地址链接传递
     * @return string|null
     */
    public function publicEncrypt($public_key, $content = '', $is_url = false)
    {
        if (!is_string($content)) {
            return null;
        }
        $result = '';
        $data = str_split($content, $this->rsa_encrypt_block_size);
        foreach ($data as $block) {
            openssl_public_encrypt($block, $dataEncrypt, self::getPublicKey($public_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataEncrypt;
        }
        if ($is_url) {
            return $result ? self::encode64($result) : null;
        }
        return $result ? base64_encode($result) : null;
    }

    /**
     * 私钥解密
     * @param string $private_key 私钥
     * @param string $encrypted 公钥加密过的字符串
     * @param boolean $is_url 是否用于地址链接传递
     * @return string|null
     */
    public function privateDecrypt($private_key, $encrypted = '', $is_url = false)
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $result = '';
        $encrypted = $is_url ? self::decode64($encrypted) : base64_decode($encrypted);
        $data = str_split($encrypted, $this->rsa_decrypt_block_size);
        foreach ($data as $block) {
            openssl_private_decrypt($block, $dataDecrypt, self::getPrivateKey($private_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataDecrypt;
        }
        return $result ? $result : null;
    }

    /**
     * 私钥解密01
     * @param string $private_key 私钥
     * @param string $encrypted 公钥加密过的字符串
     * @param boolean $is_url 是否用于地址链接传递
     * @param string $delimiter 分隔符
     * @return null|string
     */
    public function privateDecrypt_01($private_key, $encrypted = '', $is_url = true, $delimiter = '.')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $result = '';
        $encrypted = $is_url ? self::decode64($encrypted) : base64_decode($encrypted);
        if (strpos($encrypted, $delimiter) > 0) {
            $list = explode($delimiter, $encrypted);
            foreach ($list as $index => $v) {
                $block = self::decode64($v);
                openssl_private_decrypt($block, $dataDecrypt, self::getPrivateKey($private_key), OPENSSL_PKCS1_PADDING);
                $result .= $dataDecrypt;
            }
        }
        return $result ? $result : null;
    }

    /**
     * 公钥解密
     * @param string $public_key 公钥
     * @param string $encrypted 私钥加密过的字符串
     * @param boolean $is_url 是否用于地址链接传递
     * @return string|null
     */
    public function publicDecrypt($public_key, $encrypted = '', $is_url = false)
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $result = '';
        $encrypted = $is_url ? self::decode64($encrypted) : base64_decode($encrypted);
        $data = str_split($encrypted, $this->rsa_decrypt_block_size);
        foreach ($data as $block) {
            openssl_public_decrypt($block, $dataDecrypt, self::getPublicKey($public_key), OPENSSL_PKCS1_PADDING);
            $result .= $dataDecrypt;
        }
        return $result ? $result : null;
    }

    /**
     * 公钥解密01
     * @param string $public_key 公钥
     * @param string $encrypted 私钥加密过的字符串
     * @param boolean $is_url 是否用于地址链接传递
     * @param string $delimiter 分隔符
     * @return null|string
     */
    public function publicDecrypt_01($public_key, $encrypted = '', $is_url = true, $delimiter = '.')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        $result = '';
        $encrypted = $is_url ? self::decode64($encrypted) : base64_decode($encrypted);
        if (strpos($encrypted, $delimiter) > 0) {
            $list = explode($delimiter, $encrypted);
            foreach ($list as $index => $v) {
                $block = self::decode64($v);
                openssl_public_decrypt($block, $dataDecrypt, self::getPublicKey($public_key), OPENSSL_PKCS1_PADDING);
                $result .= $dataDecrypt;
            }
        }
        return $result ? $result : null;
    }

    /**
     * 私钥加签
     * @param string $data 待签名字符串
     * @param string $private_key 私钥
     * @param string|int $algorithm 签名算法如 sha256
     * @return null|string
     */
    public static function sign($data, $private_key = '', $algorithm = OPENSSL_ALGO_SHA256)
    {
        if (!self::is_string($data)) {
            return null;
        }
        $private_key_id = openssl_pkey_get_private($private_key);
        openssl_sign($data, $sign, $private_key_id, $algorithm);
        openssl_free_key($private_key_id);
        $sign = self::encode64($sign);
        return $sign;
    }

    /**
     * 公钥验签
     * @param string $data 待签名字符串
     * @param string $sign 签名
     * @param string $public_key 公钥
     * @param int|string $algorithm 签名算法如 sha256
     * @return bool|null
     */
    public static function verify($data, $sign, $public_key = '', $algorithm = OPENSSL_ALGO_SHA256)
    {
        if (!self::is_string($data)) {
            return null;
        }
        $public_key_id = openssl_pkey_get_public($public_key);
        $ok = openssl_verify($data, self::decode64($sign), $public_key_id, $algorithm);
        openssl_free_key($public_key_id);
        return $ok === 1 ? true : false;
    }

    /**
     * 生成RSA密钥对
     * @param int $key_size 密钥长度
     * @param string $openssl_conf_file_path 自定义 openssl.conf 文件的路径
     * @param null|string $pass 密钥密码
     * @return array
     */
    public static function createKeys($key_size = 1024, $openssl_conf_file_path = '', $pass = null)
    {
        //参考链接: https://www.php.net/manual/zh/function.openssl-csr-new.php
        $config = array(
            'private_key_bits' => $key_size,
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
        );
        $openssl_conf_file_path && ($config['config'] = $openssl_conf_file_path);
        $res = openssl_pkey_new($config);
        openssl_pkey_export($res, $private_key, $pass, $config);
        $details = openssl_pkey_get_details($res);
        $public_key = $details["key"];
        return array(
            'public_key' => $public_key,
            'private_key' => $private_key,
            'bits' => $details['bits'],
            'type' => $details['type'],
            'rsa' => $details['rsa'],
            'resource' => $res,
            'rsa_n' => base64_encode($details['rsa']['n']),
            'rsa_e' => bin2hex($details['rsa']['e']),
        );
    }

    /**
     * 获取私钥文件信息 pem
     * @param string $private_key
     * @return array|bool
     */
    public static function get_keys_details($private_key = '')
    {
        $details = openssl_pkey_get_details(openssl_pkey_get_private($private_key));
        if (!$details) {
            return false;
        }
        return array(
            'bits' => $details['bits'],
            'type' => $details['type'],
            'rsa' => $details['rsa'],
            'rsa_n' => base64_encode($details['rsa']['n']),
            'rsa_e' => bin2hex($details['rsa']['e']),
            'public_key' => $details['key'],
            'private_key' => $private_key,
        );
    }

    /**
     * 获取密钥长度
     * @param string $pub_pri_key 公钥或私钥字符串
     * @param int|string $op
     * @return bool|integer
     */
    public static function get_key_size($pub_pri_key = '', $op = 1)
    {
        if (in_array($op, array(1, 'public', 'public_key'), true)) {
            $key_id = openssl_pkey_get_public($pub_pri_key);
        } elseif (in_array($op, array(2, 'private', 'private_key'), true)) {
            $key_id = openssl_pkey_get_private($pub_pri_key);
        } else {
            return false;
        }
        $details = openssl_pkey_get_details($key_id);
        return $details ? $details['bits'] : false;
    }

    /**
     * 读取配置文件内容
     * @param string $data 配置文件内容或配置文件路径
     * @param bool $is_file 是否是文件路径
     * @return array|string
     */
    public static function get_config_ini($data = '', $is_file = false)
    {
        if ($is_file && self::is_string($data)) {
            $content = file_get_contents($data);
        } else {
            $content = $data;
        }
        $list = preg_split("/(\n)+/", $content);
        $data = array();
        $lv1_key = "";
        foreach ($list as $k => $v) {
            $v = trim($v);
            if ($v && !preg_match("/^\s*[#;]+/", $v)) {
                if (preg_match("/(\[\s*[A-Za-z0-9_\-.]+?\s*\])/i", $v)) {
                    $lv1_key = trim(trim($v, "[]\t\n\r\0\x0B"));
                    $data[$lv1_key] = array();
                } elseif ($lv1_key) {
                    $opt = preg_split("/\s*=\s*/", $v, 2);
                    $data[$lv1_key][trim($opt[0])] = trim($opt[1], "\"'\t\n\r\0\x0B");
                }
            }
        }
        return $data;
    }

    /**
     * 创建配置文件
     * @param string $data
     * @return string
     */
    public static function create_config_ini($data = '')
    {
        if (is_array($data) && count($data) > 0) {
            $r = array();
            foreach ($data as $k => $v) {
                $r[] = "[ $k ]\n";
                if (is_array($v)) {
                    foreach ($v as $k1 => $v1) {
                        $r[] = "$k1 = $v1\n";
                    }
                }
                $r[] = "\n";
            }
            return join('', $r);
        }
        return '';
    }

    private $cert_conf = array(
        //在证书中使用的专有名称或主题字段。
        'dn' => array(
            //所在国家
            'countryName' => 'CN',
            //所在省份
            'stateOrProvinceName' => 'HaiNan',
            //所在城市
            'localityName' => 'HaiKou',
            //注册人姓名
            'organizationName' => 'TX_XLR',
            //组织名称
            'organizationalUnitName' => 'TX',
            //公共名称
            'commonName' => 'fwf',
            //邮箱
            'emailAddress' => 'fwf@qq.com',
        ),
        'config' => array(
            'private_key_bits' => 2048,
            'private_key_type' => OPENSSL_KEYTYPE_RSA
        ),
        'private_key' => '',
        'days' => 365, //证书有效时间
        'public_cert_path' => '', //公钥证书保存路径 /public.crt
        'private_cert_path' => '', //证书私钥保存路径 /private.pfx
        'root_path' => '',
    );

    /**
     * @param string|array $name
     * @param string $val
     * @return $this
     */
    public function cert_set($name = '', $val = '')
    {
        if (self::is_string($name)) {
            $this->cert_conf[$name] = $val;
        } elseif (is_array($name)) {
            foreach ($name as $k => $v) {
                self::cert_set($k, $v);
            }
        }
        return $this;
    }

    /**
     * @param string|array $countryName
     * @param string $stateOrProvinceName
     * @param string $localityName
     * @param string $organizationName
     * @param string $organizationalUnitName
     * @param string $commonName
     * @param string $emailAddress
     * @return $this
     */
    public function cert_dn_set($countryName = 'CN', $stateOrProvinceName = 'HaiNan', $localityName = 'HaiKou', $organizationName = 'TX_XLR', $organizationalUnitName = 'TX', $commonName = 'fwf', $emailAddress = 'fwf@qq.com')
    {
        $dn = &$this->cert_conf['dn'];
        if (is_array($countryName) && count($countryName) > 0) {
            foreach ($countryName as $key => $val) {
                $dn[$key] = $val;
            }
        } else {
            $args = func_get_args();
            $dn['countryName'] = $countryName;
            $dn['stateOrProvinceName'] = $stateOrProvinceName;
            $dn['localityName'] = $localityName;
            $dn['organizationName'] = $organizationName;
            $dn['organizationalUnitName'] = $organizationalUnitName;
            $dn['commonName'] = $commonName;
            $dn['emailAddress'] = $emailAddress;
            for ($i = 7, $len = count($args); $i < $len; $i++) {
                $val = $args[$i];
                if (is_array($val) && count($val) > 0) {
                    foreach ($val as $k1 => $v1) {
                        $dn[$k1] = $v1;
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @param string $name
     * @param string $val
     * @param string $key_size
     * @param string $openssl_conf_file_path
     * @return $this
     */
    public function cert_config_set($name = '', $val = '', $key_size = '', $openssl_conf_file_path = '')
    {
        $c = &$this->cert_conf['config'];
        if (self::is_string($name)) {
            $c[$name] = $val;
            self::is_string($key_size) && ($c['private_key_bits'] = $key_size);
            self::is_string($openssl_conf_file_path) && ($c['config'] = $openssl_conf_file_path);
        } elseif (is_array($name) && count($name) > 0) {
            foreach ($name as $k => $v) {
                self::cert_config_set($k, $v);
            }
        }
        return $this;
    }

    public function public_cert_path($path){
        return self::cert_set('public_cert_path',$path);
    }
    public function private_cert_path($path){
        return self::cert_set('private_cert_path',$path);
    }

    public function certificate($privateKey = '', $pass = null, $digest_alg = 'sha256')
    {
        $cc = &$this->cert_conf;
        $config = $cc['config'];
        $dn = $cc['dn'];
        if (!$privateKey) {
            $res = self::createKeys($config['private_key_bits'], $config['config'], $pass);
            $privateKey = $res['private_key'];
        }
        $privateKey = self::getPrivateKey($_privateKey = $privateKey);
        $config['digest_alg'] = $digest_alg;
        $csr = openssl_csr_new($dn, $privateKey, $config);
        //参考：https://www.php.net/manual/zh/function.openssl-csr-sign.php 【证书签名】
        $x509 = openssl_csr_sign($csr, null, $privateKey, $this->cert_conf['days'], $config);
        //将公钥证书存储到一个变量 $csrkey，由 PEM 编码格式命名
        openssl_x509_export($x509, $csrkey);
        //把私钥转换为 PKCS12 文件格式的字符串
        openssl_pkcs12_export($x509, $privateKey_pkcs12, $privateKey, $pass);
        //导出证书公钥
        //openssl_x509_export_to_file($x509,getcwd().'/csr.public.cert');
        //导出证书私钥
        //openssl_pkcs12_export_to_file($x509, 'csr.private.pfx', $privateKey, $pass);

        if (self::is_string($cc['public_cert_path'])) {
            file_put_contents($cc['public_cert_path'], $csrkey);
        }
        if (self::is_string($cc['private_cert_path'])) {
            file_put_contents($cc['private_cert_path'], $privateKey_pkcs12);
        }
        return array('public_cert' => $csrkey, 'private_pkcs12' => $privateKey_pkcs12, 'private_key' => $_privateKey);
    }


    /**
     * 判断是否存在
     * @param $var
     * @param array $opt 这里的值代表了不存在的意思
     * @return bool
     */
    public static function is_exist($var, $opt = array("", null, false))
    {
        isset($var) || ($var = false);
        return !in_array($var, $opt, true);
    }

    /**
     * 是否是字符串
     * @param string|integer|double $str
     * @return bool
     */
    public static function is_string($str = '')
    {
        return !in_array($str, array('', null, false), true) && (is_string($str) || is_numeric($str));
    }

    /**
     * 获取变量值
     * @return bool|*
     */
    public static function get_vars()
    {
        $args = func_get_args();
        $var = false;
        foreach ($args as $arg) {
            if (self::is_exist($arg)) {
                $var = $arg;
                break;
            }
        }
        return $var;
    }

    /**
     * 可以在url里传递
     * @param $data
     * @return string
     */
    public static function encode64($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * @param $data
     * @return bool|string
     */
    public static function decode64($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    /**
     * 返回毫秒
     * @return float
     */
    public static function m_second()
    {
        $t = strval(microtime());
        $l = explode(' ', $t);
        $s = $l[1];
        $ms = ~~(floatval($l[0]) * 1000);
        $v = strval($s) . str_pad(strval($ms), 3, '0', STR_PAD_LEFT);
        return (float)$v;
    }

    /**
     * 返回微秒
     * @return string
     */
    public static function u_second()
    {
        $t = strval(microtime());
        $l = explode(' ', $t);
        $s = $l[1];
        $us = ~~(floatval($l[0]) * 1000000);
        $v = strval($s) . str_pad(strval($us), 6, '0', STR_PAD_LEFT);
        return $v;
    }

    /**
     * 设置数组, 如: set_array($arr,'lv1.lv2',$value); set_array($arr,array('lv1','lv2'),$value);
     * @param $arr
     * @param string $name
     * @param string $value
     * @param int $index
     * @return mixed
     */
    public static function set_array(&$arr, $name = '', $value = '', $index = 0)
    {
        if ($name && is_array($name)) {
            $len = count($name);
            if ($len > 0 && $index < $len) {
                if (!isset($arr[$name[$index]]) || !is_array($arr[$name[$index]])) {
                    $arr[$name[$index]] = array();
                    if ($index == $len - 1) {
                        $arr[$name[$index]] = $value;
                        return $arr;
                    }
                }
                self::set_array($arr[$name[$index]], $name, $value, $index + 1);
            }
        } elseif ($name && is_string($name) && strpos($name, '.') > 0) {
            $name = explode('.', $name);
            self::set_array($arr, $name, $value, $index);
        } else {
            $arr[$name] = $value;
        }
    }

    /**
     * 获取数组 如： get_array($arr,'lv1.lv2'); get_array($arr,array('lv1','lv2'));
     * @param $arr
     * @param $name
     * @param int $index
     * @return mixed
     */
    public static function get_array(&$arr, $name, $index = 0)
    {
        if ($name && is_array($name)) {
            $len = count($name);
            if ($len > 0 && $index < $len) {
                if ($index == $len - 1) {
                    return $arr[$name[$index]];
                }
                return self::get_array($arr[$name[$index]], $name, (int)$index + 1);
            }
        } elseif ($name && is_string($name) && strpos($name, '.') > 0) {
            $name = explode('.', $name);
            return self::get_array($arr, $name, $index);
        } else {
            return $name ? $arr[$name] : $arr;
        }
    }
}