<?php

namespace app\common\controller\publics;

use think\Controller;
use think\Request;

/* vim: set expandtab tabstop=4 shiftwidth=4: */
// +----------------------------------------------------------------------+
// | PHP version 5                                                        |
// +----------------------------------------------------------------------+
// | Copyright (c) 1997-2004 The PHP Group                                |
// +----------------------------------------------------------------------+
// | This source file is subject to version 3.0 of the PHP license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.php.net/license/3_0.txt.                                  |
// | If you did not receive a copy of the PHP license and are unable to   |
// | obtain it through the world-wide-web, please send a note to          |
// | license@php.net so we can mail you a copy immediately.               |
// +----------------------------------------------------------------------+
// | Authors: Original Author <author@example.com>                        |
// |          Your Name <you@example.com>                                 |
// +----------------------------------------------------------------------+
//
// $Id:$

class Mobile
{
    /**
     * 获取手机号码
     * 函数返回值: 成功返回号码，失败返回false
     * @return bool|null|string|string[]
     */
    function getPhoneNumber()
    {
        if (isset($_SERVER['HTTP_X_NETWORK_INFO'])) {
            $str1 = $_SERVER['HTTP_X_NETWORK_INFO'];
            $getstr1 = preg_replace('/(.*,)(11[d])(,.*)/i', '', $str1);
            Return $getstr1;
        } elseif (isset($_SERVER['HTTP_X_UP_CALLING_LINE_ID'])) {
            $getstr2 = $_SERVER['HTTP_X_UP_CALLING_LINE_ID'];
            Return $getstr2;
        } elseif (isset($_SERVER['HTTP_X_UP_SUBNO'])) {
            $str3 = $_SERVER['HTTP_X_UP_SUBNO'];
            $getstr3 = preg_replace('/(.*)(11[d])(.*)/i', '', $str3);
            Return $getstr3;
        } elseif (isset($_SERVER['DEVICEID'])) {
            Return $_SERVER['DEVICEID'];
        } else {
            Return false;
        }
    }

    /**
     * 函数功能: 取头信息
     * 函数返回值: 成功返回号码，失败返回false
     * @return string
     */
    function getHttpHeader()
    {
        $str = '';
        foreach ($_SERVER as $key => $val) {
            $gstr = str_replace("&", "&", $val);
            $str .= "$key -> " . $gstr . " ";
        }
        Return $str;
    }

    /**
     * 函数功能: 取UA
     * 函数返回值: 成功返回号码，失败返回false
     * @return bool
     */
    function getUA()
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            Return $_SERVER['HTTP_USER_AGENT'];
        } else {
            Return false;
        }
    }

    /**
     * 函数功能: 取得手机类型
     * 函数返回值: 成功返回string，失败返回false
     * @return bool
     */
    function getPhoneType()
    {
        $ua = $this->getUA();
        if ($ua != false) {
            $str = explode(' ', $ua);
            Return $str[0];
        } else {
            Return false;
        }
    }

    /**
     * 函数功能: 判断是否是opera
     * 函数返回值: 成功返回string，失败返回false
     * @return bool
     */
    function isOpera()
    {
        $uainfo = $this->getUA();
        if (preg_match('/.*Opera.*/i', $uainfo)) {
            Return true;
        } else {
            Return false;
        }
    }

    /**
     * 函数功能: 判断是否是m3gate
     * 函数返回值: 成功返回string，失败返回false
     * @return bool
     */
    function isM3gate()
    {
        $uainfo = $this->getUA();
        if (preg_match('/M3Gate/i', $uainfo)) {
            Return true;
        } else {
            Return false;
        }
    }

    /**
     * 函数功能: 取得HA
     * 函数返回值: 成功返回string，失败返回false
     * @return bool
     */
    function getHttpAccept()
    {
        if (isset($_SERVER['HTTP_ACCEPT'])) {
            Return $_SERVER['HTTP_ACCEPT'];
        } else {
            Return false;
        }
    }

    /**
     * 函数功能: 取得手机IP
     * 函数返回值: 成功返回string
     * @return array|false|string
     */
    function getIP()
    {
        $ip = getenv('REMOTE_ADDR');
        $ip_ = getenv('HTTP_X_FORWARDED_FOR');
        if (($ip_ != "") && ($ip_ != "unknown")) {
            $ip = $ip_;
        }
        return $ip;
    }
}

