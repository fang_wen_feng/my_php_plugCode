<?php
/**
 * 功能描述：数据库操作
 * 作者：方文锋
 * 电话号码：18876126389
 * 个人邮箱：2292118369@qq.com  fang2292118369@gmail.com
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/4
 * Time: 12:56
 */
defined('BASEPATH') OR exit('非法访问!');

class db1 extends CI_Model
{
    protected $key = "";
    private $tableName = "";
    private $fields = "*";
    private $joins = "";
    private $wheres = "";
    private $groupBys = "";
    private $orders = "";
    private $limits = "";
    private $type = array(
        "insert" => "",
        "update" => "",
    );
    private $getSql = false;


    public function __construct()
    {
        //父类继承
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    /**
     * 把一维数组转换为特定字符串内容（用于sql语句）
     * @param string $lv1_array
     * @param string $name
     * @return string
     */
    public function array2string($lv1_array = '', $name = 'v')
    {
        $k = '';
        $v = '';
        $fields = '';
        if (is_array($lv1_array) && count($lv1_array) > 0) {
            foreach ($lv1_array as $key => $value) {
                $k .= "$key,";
                $v .= is_int($value) || is_float($value) ? "$value," : "'$value',";
                $fields .= is_int($value) || is_float($value) ? "$key=$value," : "$key='$value',";
            }
            $data['k'] = $k = trim($k, ',');
            $data['v'] = $v = trim($v, ',');
            $data['fields'] = $fields = trim($fields, ',');
            $data['insert'] = " ( $k )values( $v ) ";
            $data['update'] = " $fields ";
            $name = strtolower($name);
            return in_array($name, array('k', 'v', 'fields', 'insert', 'update')) ? $data[$name] : $data;
        } else {
            return '';
        }
    }

    /**
     * 数据过滤函数
     * @param string|array $data 待过滤的字符串或字符串数组
     * @param bool $force 为true时忽略get_magic_quotes_gpc
     * @param bool $is_htmlspecialchars 为true时，防止被挂马，跨站攻击
     * @param bool $regexp 正则匹配转义字符
     * @return array|null|string|string[]
     */
    public function input($data, $force = false, $is_htmlspecialchars = false, $regexp = false)
    {
        if (is_string($data)) {
            $data = trim($is_htmlspecialchars ? htmlspecialchars($data) : $data);
            if (($force == true) || (!get_magic_quotes_gpc())) {
                $data = addslashes($data); // 防止sql注入
            }
            if ($regexp) {
                if (is_array($regexp)) {
                    $regexp = join('|', $regexp);
                }
                $data = preg_replace('/(' . $regexp . ')/', '\\\\$1', $data);
            }
            return $data;
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = self::input($value, $force, $is_htmlspecialchars, $regexp);
            }
            return $data;
        } else {
            return $data;
        }
    }

    /**
     * 浏览器友好的变量输出
     * @param mixed $var 变量
     * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
     * @param string $label 标签 默认为空
     * @param boolean $strict 是否严谨 默认为true
     * @return void|string
     */
    public function dump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            } else {
                $output = $label . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else {
            return $output;
        }
    }

    /**
     * 获取数据类型
     * @param $var
     * @return string
     */
    public function get_type($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_callable($var)) return "function reference";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }

    /**
     * 返回数组的维度
     * @param $arr
     * @return mixed
     */
    public function arrayLevel($arr)
    {
        $al = array(0);
        self::aL($arr, $al);
        return max($al);
    }

    /**
     * 配合方法 self::arrayLevel
     * @param $arr
     * @param $al
     * @param int $level
     */
    private function aL($arr, &$al, $level = 0)
    {
        if (is_array($arr)) {
            $level++;
            $al[] = $level;
            foreach ($arr as $k => $v) {
                self::aL($v, $al, $level);
            }
        }
    }

    /**
     * 判断是否是关联数组
     * @param $arr
     * @return bool
     */
    public function is_assoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * sql语句参数绑定（主要在 where 部分）
     * @param string $strings
     * @param array $bind
     * @return mixed|string
     */
    public function where_param_bind($strings = '', $bind = array())
    {
        if ($strings && is_string($strings)) {
            if (is_array($bind) && count($bind) > 0) {
                foreach ($bind as $key => $value) {
                    if (is_int($value) || is_float($value)) {
                        $strings = str_replace(":$key", "$value", $strings);
                    } else {
                        $strings = str_replace(":$key", "'$value'", $strings);
                    }
                }
            }
        }
        return $strings;
    }

    /**
     * 初始化变量（sql语句拼接）
     */
    public function init()
    {
        $this->fields = "*";
        $this->joins = "";
        $this->wheres = "";
        $this->groupBys = "";
        $this->orders = "";
        $this->limits = "";
        $this->type['insert'] = "";
        $this->type['update'] = "";

        $this->getSql = false;
        return $this;
    }

    /**
     * 构建 where 条件语句
     * @param string|array $field
     * @param string|array $options
     * @param string|array $condition
     * @return $this
     */
    public function where($field = '', $options = '', $condition = '')
    {
        $args = func_get_args();
        $args_len = count($args);
        $where_str = '';
        if (is_array($field) && in_array(strtolower($options), array('or', 'and')) && !self::exist($condition)) {
            //$field 为数组的时候（1-3维数组）
            $arr_lv = self::arrayLevel($field);
            if ($arr_lv > 0 && $arr_lv < 4) {
                foreach ($field as $k => $v) {
                    if (is_array($v) && count($v) > 0) {
                        //['字段名', '选项表达式(=|<|>|<>|like|not like|regexp|not regexp|....)', '值', 'and|or']
                        $f = $v[0];
                        $opt = $v[1];
                        $val = $v[2];
                        $or_and = $v[3] ? strtoupper($v[3]) : 'AND';
                        if (in_array(strtolower(trim($opt)), array('in', 'not in'))) {
                            if (is_array($val)) {
                                $val = self::array2string($val, 'v');
                                $where_str .= "$f $opt ($val) $or_and ";
                            } else {
                                continue;
                            }
                        } else {
                            $where_str .= is_int($val) || is_float($val) ? "$f $opt $val $or_and " : "$f $opt '$val' $or_and ";
                        }
                    } else {
                        $or_and = $options ? strtoupper($options) : 'AND';
                        $where_str .= is_int($v) || is_float($v) ? "$k = $v $or_and " : "$k = '$v' $or_and ";
                    }
                }
                $where_str = rtrim(rtrim($where_str), $or_and);
                $this->wheres .= "($where_str)";
            }
        } elseif ($field && is_string($field) && in_array(strtolower($args[$args_len - 1]), array('or', 'and'))) {
            if ($args_len >= 3) {
                //同一字段多个查询条件,如： $this->where('name', ['like','abc%'], ['like','%haha%'], 'or')
                $or_and = strtoupper($args[$args_len - 1]);
                for ($i = 1, $len = $args_len - 1; $i < $len; $i++) {
                    if (is_array($args[$i]) && count($args[$i]) > 1) {
                        $opt = $args[$i][0];
                        $val = $args[$i][1];
                        if (is_array($val) && in_array(strtolower($opt), array('in', 'not in'))) {
                            $val = self::array2string($val, 'v');
                            $where_str .= "$field $opt ($val) $or_and ";
                        } elseif (is_int($val) || is_float($val)) {
                            $where_str .= "$field $opt $val $or_and ";
                        } else {
                            $where_str .= "$field $opt '$val' $or_and ";
                        }
                    }
                }
                $where_str = rtrim(rtrim($where_str), $or_and);
                $this->wheres .= "($where_str)";
            }
        } elseif (preg_match('/(\&|\|)/i', $field) && self::exist($options) && is_string($options) && self::exist($condition) && (is_array($condition) || is_string($condition))) {
            //多字段相同查询条件,如：$this->where('name|title', 'like', 'abc%');  $this->where('name&title', 'like', '%haha%');
            $preg = '/([a-zA-z0-9_]+\||[a-zA-z0-9_]+\&|[a-zA-z0-9_]+)/i';
            preg_match_all($preg, $field, $lv2_array);
            foreach ($lv2_array[0] as $k => $v) {
                if (strpos($v, '|') > 0) {
                    $new_array[] = array('field' => trim(trim($v), '|'), 'or_and' => 'OR', 'pre' => '|');
                } elseif (strpos($v, '&') > 0) {
                    $new_array[] = array('field' => trim(trim($v), '&'), 'or_and' => 'AND', 'pre' => '&');
                } else {
                    $new_array[] = array('field' => trim($v), 'or_and' => 'OR', 'pre' => '');
                }
            }
            foreach ($new_array as $k => $v) {
                $f = $v['field'];
                $or_and = $v['or_and'];
                if (is_array($condition) && in_array(strtolower($options), array('in', 'not in'))) {
                    $_condition = self::array2string($condition, 'v');
                    $where_str .= "$f $options ($_condition) $or_and ";
                } elseif (is_int($condition) || is_float($condition)) {
                    $where_str .= "$f $options $condition $or_and ";
                } else {
                    $where_str .= "$f $options '$condition' $or_and ";
                }
            }
            $where_str = rtrim(rtrim($where_str), $or_and);
            $this->wheres .= "($where_str)";

        } elseif ($field && is_string($field) && !is_object($options) && !self::exist($condition)) {
            if (is_array($options)) {
                //where 的字符串条件查询，也可以进行参数绑定(如： $this->where("field = :name", array("name"=>"value") ))
                $where_str = self::where_param_bind($field, $options);
            } elseif (self::exist($options) && !is_array($options)) {
                $where_str = is_int($options) || is_float($options) ? "$field = $options" : "$field = '$options'";
            } else {
                $where_str = $field;
            }
            $where_str = rtrim($where_str);
            $this->wheres .= "($where_str)";
        } else {
            if (is_array($condition) && in_array(strtolower($options), array('in', 'not in'))) {
                $condition = self::array2string($condition, 'v');
                $options = strtoupper($options);
                $this->wheres .= "$field $options ($condition) ";
            } elseif (is_int($condition) || is_float($condition)) {
                $this->wheres .= "$field $options $condition ";
            } else {
                $this->wheres .= "$field $options '$condition' ";
            }
        }
        $this->wheres = $this->wheres . " AND ";
        return $this;
    }

    public function whereOr()
    {
        $args = func_get_args();
        call_user_func_array(array($this, 'where'), $args);
        $this->wheres = rtrim(rtrim($this->wheres), 'AND') . 'OR ';
        return $this;
    }

    /**
     * 左连接
     * @param string $join 如：tableName t
     * @param string $condition 如：t.id = a.id
     * @return $this
     */
    public function leftJoin($join = '', $condition = '')
    {
        $this->joins .= "LEFT JOIN $join ON $condition ";
        return $this;
    }

    /**
     * 右连接
     * @param string $join 如：tableName t
     * @param string $condition 如：t.id = a.id
     * @return $this
     */
    public function rightJoin($join = '', $condition = '')
    {
        $this->joins .= "RIGHT JOIN $join ON $condition ";
        return $this;
    }

    /**
     * 连接
     * @param string $join
     * @param string $condition
     * @return $this
     */
    public function join($join = '', $condition = '')
    {
        $this->joins .= "JOIN $join ON $condition ";
        return $this;
    }

    /**
     * 截取数据范围
     * @param string|int $offset 开始位置（从0开始）
     * @param string|int $length 截取的长度
     * @return $this
     */
    public function limit($offset = '', $length = '')
    {
        if ($offset !== '' || intval($offset) >= 0) {
            if (!empty($length)) {
                $this->limits = "LIMIT $offset ,$length";
            } else {
                $this->limits = "LIMIT $offset";
            }
        }
        return $this;
    }

    /**
     * 排序
     * @param string|array $field 字段名，数组时必须是一维关联数组
     * @param string $order 排序类型(desc|ASC)
     * @return $this
     */
    public function order($field = '', $order = '')
    {
        if (is_array($field)) {
            foreach ($field as $key => $value) {
                $this->orders .= "$key $value ,";
            }
        } else {
            $this->orders .= "$field $order ,";
        }
        return $this;
    }

    /**
     * 分组
     * @param string $field
     * @param bool $with_rollup 可以实现在分组统计数据基础上再进行相同的统计（SUM,AVG,COUNT…）
     * @return $this
     */
    public function groupBy($field = '', $with_rollup = false)
    {
        $this->groupBys = rtrim(" $field");
        ($with_rollup) and ($this->groupBys = " $field WITH ROLLUP");
        return $this;
    }

    /**
     * 表名
     * @param string $tableName 表名（tableName、db.tableName）
     * @return $this
     */
    public function table($tableName = '')
    {
        self::init();
        $this->tableName = " $tableName ";
        return $this;
    }

    public function name($name)
    {
        self::init();
        return self::table($this->config['table_prefix'] . $name);
    }

    /**
     * 查询的字段
     * @param string $field 字段（field1,field2,t.*,c.f1,c.f2..）
     * @return $this
     */
    public function field($field = '')
    {
        $this->fields = !empty($field) ? $field : "*";
        return $this;
    }

    /**
     * 更新语句(更新的数据需要使用SQL函数)
     * @param string|array $field
     * @param string $value
     * @return $this
     */
    public function exp($field = '', $value = '')
    {
        if (is_array($field)) {
            foreach ($field as $key => $v) {
                $this->exp($key, $v);
            }
        } elseif ($field !== '' && $value !== '') {
            $this->type['update'] = ltrim(trim($this->type['update']), ',');
            $this->type['update'] .= ",$field=$value";
        }
        return $this;
    }

    /**
     * 插入和修改的条件语句整理
     * @param array|string $field 推荐传入数组(一维关联数组)
     * @param null|string $value
     * @return $this
     */
    public function data($field, $value = null)
    {
        if (is_array($field)) {
            $data = self::array2string($field, null);
            $this->type['insert'] = $data['insert'];
            $this->type['update'] = $data['update'];
        } else {
            if (!empty($field) && !empty($value)) {
                $this->type['insert'] = "($field)VALUES($value)";
                $this->type['update'] = "$field='$value'";
            } elseif (!empty($field) && empty($value)) {
                $this->type['update'] = $this->type['insert'] = $field;
            } else {
                $this->type['update'] = $this->type['insert'] = "";
            }
        }
        return $this;
    }


    /**
     * 多条记录查询
     * @return array 返回二维数组或者空数组（[]）
     */
    public function select()
    {
        $sql = $this->structure_select();
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            $arr = $re->result_array();
            if (count($arr) < 1) {
                return false;
            }
            return $arr;
        } else {
            return false;
        }
    }

    /**
     * 获取一条数据 （一维关联数组）
     * @param $rowNum number 选中的行 （0为第一条，该参数不用填写）
     * @return array 返回一维关联数组
     */
    public function find($rowNum = 0)
    {
        $this->limit(0, 1);
        $sql = $this->structure_select();
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            return $re->row_array($rowNum);
        } else {
            return false;
        }

    }

    /**
     * 获取查询返回的总条数
     * @param $field string 字段名(单个字段)
     * @return number
     */
    public function count($field = '*')
    {
        $this->sql_str_exec();
        $this->fields = $this->fields && $this->fields != "*" ? rtrim(trim($this->fields), ",") . "," : "";
        $num = "num_" . time();
        $sql = "SELECT {$this->fields}COUNT($field) AS $num FROM {$this->tableName} {$this->joins} {$this->wheres} {$this->orders} {$this->limits}";
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            return $re->row_array(0)[$num];
        } else {
            return 0;
        }
    }

    /**
     * 获取某个字段的值
     * @param $field string 字段名（单个字段）
     * @return string 返回字段值
     */
    public function value($field = '')
    {
        if (empty($field)) {
            return false;
        }
        $this->limit(0, 1);
        $sql = self::structure_select();
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            return $re->row_array(0)[$field];
        } else {
            return false;
        }
    }

    public function insert($data = '', $get_affected_rows = 0)
    {
        if (!empty($data)) {
            $this->data($data);
        }
        $sql = "INSERT INTO {$this->tableName}{$this->type['insert']}";
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            if ($get_affected_rows == 1) {
                return $this->db->affected_rows();
            }
            return $re; //Boolean
        }
        return false;
    }

    public function update($data = '', $get_affected_rows = 0)
    {
        if (!empty($data)) {
            $this->data($data);
        }
        $sql = self::structure_update();
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            if ($get_affected_rows == 1) {
                return $this->db->affected_rows();
            }
            return $re; //Boolean
        }
        return false;
    }

    public function delete()
    {
        $sql = self::structure_select();
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            return $this->db->affected_rows();
            //return $re; //Boolean
        }
        return $re; //Boolean
    }

    /**
     * 插入并获取插入id (获取新增的 数据表表里面的自增字段的值)
     * @param $data string|array 可以是字符串和一维关联数组
     * @param2 ....
     */
    public function insertGetId($data = '')
    {
        if (!empty($data)) {
            $this->data($data);
        }
        $sql = "INSERT INTO {$this->tableName}{$this->type['insert']}";
        if ($this->getSql == true) {
            return $sql;
        }
        $re = $this->db->query($sql);
        if ($re) {
            return $this->db->insert_id();
        }
        return false;
    }

    /**
     * 获取sql语句
     * @param $boolean boolean 是否获取sql语句
     */
    public function get_sql($boolean = true)
    {

        //$this->orders = "ORDER BY ".substr($this->orders,0,strlen($this->orders)-1);
        //$this->wheres = substr($this->wheres,0,strlen($this->wheres)-4);
        //return "{$this->joins} WHERE {$this->wheres} {$this->orders} {$this->limits}";
        $this->getSql = $boolean;
        return $this;
    }

    /*
     * 从数据库获取多行数据 （二维数组）
     * @param $sql string 查询语句
     * */
    public function sql_select($sql)
    {
        $re = $this->db->query($sql);
        if ($re) {
            $arr = $re->result_array();
            if (count($arr) < 1) {
                return false;
            }
            return $arr;
        } else {
            return false;
        }
    }

    /*
     * 从数据库查询一行数据（一维关联数组）
     * @param $sql string 查询语句
     * */
    public function sql_getRow($sql, $rowNum = 0)
    {
        $re = $this->db->query($sql);
        if ($re) {
            return $re->row_array($rowNum);
        } else {
            return false;
        }
    }

    /*
     * 插入数据
     * @param $sql string|array 插入语句或者一维关联数组
     * @param $table string 表名（当$sql为数组时有效）
     * @is_return_insert_id number (1=返回插入的id)
     * */
    public function sql_insert($sql, $table, $is_return_insert_id = 1, $type = 1)
    {
        if (is_array($sql) && !empty($table)) {
            $k = "";
            $v = "";
            $field = "";
            foreach ($sql as $key => $value) {
                $k .= "$key,";
                $v .= is_int($value) || is_float($value) ? "$value," : "'$value',";
                $field .= is_int($value) || is_float($value) ? "$key=$value," : "$key='$value',";
            }
            $k = substr($k, 0, strlen($k) - 1);
            $v = substr($v, 0, strlen($v) - 1);
            $field = substr($field, 0, strlen($field) - 1);
            $sql_str = "INSERT INTO $table( $k )values( $v )";
            $sql_strs = "INSERT INTO $table SET $field ";
            $re = $this->db->query($type == 1 ? $sql_str : $sql_strs);
        } else {
            $re = $this->db->query($sql);
        }
        if ($re) {
            if ($is_return_insert_id == 1) {
                return $this->db->insert_id();
            }
            return $this->db->affected_rows();
        }
        return false;
    }

    public function sql_update($sql = '', $tableName = '', $wheres = '', $is_affected_rows = 1)
    {
        if (is_array($sql) && !empty($tableName)) {
            $field = "";
            foreach ($sql as $key => $value) {
                if (is_int($value) || is_float($value)) {
                    $field .= "$key=$value,";
                } else {
                    $field .= "$key='$value',";
                }
            }
            $field = substr($field, 0, strlen($field) - 1);
            $sql_str = "UPDATE $tableName SET $field $wheres ";
            $re = $this->db->query($sql_str);

        } else {
            $re = $this->db->query($sql);
        }
        if ($re) {
            if ($is_affected_rows == 1) {
                return $this->db->affected_rows();
            }
            return $re;
        }
        return false;
    }

    public function sql_delete($sql, $is_affected_rows = 1)
    {
        $re = $this->db->query($sql);
        if ($re) {
            if ($is_affected_rows == 1) {
                return $this->db->affected_rows();
            }
            return $re;
        }
        return false;
    }

    /**
     * @param string $sql
     * @param null|array $where_param_bind
     * @return bool|array
     */
    public function query($sql = '', $where_param_bind = null)
    {
        if (preg_match('/^(select)/i', $sql)) {
            if (is_array($where_param_bind) && self::arrayLevel($where_param_bind) < 2 && count($where_param_bind) > 0) {
                $sql = self::where_param_bind($sql, $where_param_bind);
            }
            $re = $this->db->query($sql);
            if ($re) {
                $arr = $re->result_array();
                if (count($arr) < 1) {
                    return false;
                }
                return $arr;
            } else {
                return false;
            }
        } else {
            die('错误！只能使用 select 语句');
        }
    }

    /**
     * @param string $sql
     * @param null|array $param_bind
     * @param int $is_affected_rows
     * @return bool
     */
    public function exec($sql = '', $param_bind = null, $is_affected_rows = 1)
    {
        $sql = self::where_param_bind($sql, $param_bind);
        $re = $this->db->query($sql);
        if ($re) {
            if ($is_affected_rows == 1) {
                return $this->db->affected_rows();
            }
            return $re;
        }
        return false;
    }

    /**
     * 错误处理，返回布尔值。true=出错,false=正确
     * @return mixed
     */
    public function error(){
        return $this->db->trans_status() === false;
    }


    /**
     * 获取 where 条件语句
     * @return string
     */
    public function get_where_str()
    {
        return $this->wheres;
    }

    /**
     * 构建 select 查询语句
     * @return string
     */
    protected function structure_select()
    {
        self::sql_str_exec();
        return $sql = "SELECT {$this->fields} FROM {$this->tableName} {$this->joins} {$this->wheres} {$this->groupBys} {$this->orders} {$this->limits}";
    }

    /**
     * 构建 insert 插入语句
     * @return string
     */
    protected function structure_insert()
    {
        $sql = "INSERT INTO {$this->tableName}{$this->type['insert']}";
        return $sql;
    }

    /**
     * 构建 update 更改语句
     * @return string
     */
    protected function structure_update()
    {
        self::sql_str_exec();
        //去掉首位和末尾的字符","
        $this->type['update'] = empty($this->type['update']) ? '' : trim(trim($this->type['update']), ',');
        $sql = "UPDATE {$this->tableName} SET {$this->type['update']} {$this->wheres} {$this->orders} {$this->limits}";
        return $sql;
    }

    /**
     * 构建 delete 删除语句
     * @return string
     */
    protected function structure_delete()
    {
        self::sql_str_exec();
        $sql = "DELETE FROM {$this->tableName} {$this->wheres} {$this->orders} {$this->limits}";
        return $sql;
    }

    /**
     * 处理sql语句的拼接
     * @return object
     */
    protected function sql_str_exec()
    {
        $this->orders = empty($this->orders) ? '' : 'ORDER BY ' . rtrim(rtrim($this->orders), ',');
        $this->wheres = empty($this->wheres) ? '' : 'WHERE ' . preg_replace('/(and|or)$/i', '', rtrim($this->wheres));
        $this->groupBys = empty($this->groupBys) ? '' : 'GROUP BY ' . $this->groupBys;
        return $this;
    }

    /**
     * 自定义判断某个变量值是否存在
     * @param $vars
     * @param array $exclude_value 排除项，值为这些的时候也当作存在
     * @return bool
     */
    public function exist($vars, $exclude_value = array(0, null, '0'))
    {
        if (is_numeric($vars) || is_int($vars) || is_float($vars)) {
            return true;
        }
        if (is_array($exclude_value) && count($exclude_value) > 0) {
            foreach ($exclude_value as $index => $item) {
                if ($vars === $item) {
                    return true;
                    break;
                }
            }
        }
        return $vars ? true : false;
    }

}